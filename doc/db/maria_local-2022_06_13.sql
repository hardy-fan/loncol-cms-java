-- MariaDB dump 10.19  Distrib 10.6.4-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: loncol_cms_v2
-- ------------------------------------------------------
-- Server version	10.6.4-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT '父级id',
  `perm_type` char(1) DEFAULT NULL COMMENT '资源类型',
  `perm_name` varchar(100) NOT NULL COMMENT '权限名称',
  `perm_desc` varchar(500) DEFAULT NULL COMMENT '资源描述',
  `icon` varchar(255) DEFAULT NULL COMMENT '资源图标',
  `mark` varchar(255) DEFAULT NULL COMMENT '资源标识',
  `url` varchar(255) DEFAULT NULL COMMENT '访问路径',
  `sort` smallint(6) DEFAULT 100 COMMENT '排序',
  `status` tinyint(4) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb3 COMMENT='权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` (`id`, `parent_id`, `perm_type`, `perm_name`, `perm_desc`, `icon`, `mark`, `url`, `sort`, `status`) VALUES (1,0,'2','工作台','工作台','fas fa-home','workdest','/workdest',1,1),(3,24,'2','用户管理','用户管理','fas fa-user','users','/users',1,1),(4,3,'3','列表查询','用户列表查询',NULL,'user:list','/user/list',0,1),(5,3,'3','新增','新增用户',NULL,'user:add','/user/add',0,1),(6,3,'3','编辑','编辑用户',NULL,'user:edit','/user/edit',0,1),(7,3,'3','删除','删除用户',NULL,'user:delete','/user/delete',0,1),(8,3,'3','批量删除','批量删除用户','','user:batchDelete','/user/batch/delete',0,1),(9,3,'3','分配角色','分配角色',NULL,'user:assignRole','/user/assign/role',0,1),(10,24,'2','角色管理','角色管理','fas fa-user-friends','roles','/roles',2,1),(11,10,'3','列表查询','角色列表查询',NULL,'role:list','/role/list',0,1),(12,10,'3','新增','新增角色',NULL,'role:add','/role/add',0,1),(13,10,'3','编辑','编辑角色',NULL,'role:edit','/role/edit',0,1),(14,10,'3','删除','删除角色',NULL,'role:delete','/role/delete',0,1),(15,10,'3','批量删除','批量删除角色','','role:batchDelete','/role/batch/delete',0,1),(16,10,'3','分配权限','分配权限',NULL,'role:assignPerms','/role/assign/permission',0,1),(17,24,'2','权限管理','资源管理','fas fa-clipboard-list','permissions','/permissions',3,1),(18,17,'3','列表查询','资源列表',NULL,'permission:list','/permission/list',0,1),(19,17,'3','新增','新增资源',NULL,'permission:add','/permission/add',0,1),(20,17,'3','编辑','编辑资源',NULL,'permission:edit','/permission/edit',0,1),(21,17,'3','删除','删除资源',NULL,'permission:delete','/permission/delete',0,1),(22,0,'1','运维管理','运维管理','fas fa-sliders-h',NULL,NULL,7,1),(23,22,'2','数据监控','数据监控','fas fa-desktop','database','/database/monitoring',1,1),(24,0,'1','系统管理','系统管理','fas fa-cog',NULL,NULL,5,1),(25,24,'2','在线用户','在线用户','fas fa-user-check','onlineUsers','/online/users',4,1),(32,25,'3','在线用户查询','在线用户查询','','onlineUser:list','/online/user/list',0,1),(33,25,'3','踢出用户','踢出用户','','onlineUser:kickout','/online/user/kickout',0,1),(34,25,'3','批量踢出','批量踢出','','onlineUser:batchKickout','/online/user/batch/kickout',0,1),(35,0,'1','网站管理','网站管理','fas fa-columns',NULL,NULL,3,1),(36,35,'2','基础信息','基础设置','fas fa-info-circle','siteinfo','/siteinfo',1,1),(37,36,'3','保存','基础设置-保存','','siteinfo:save','/siteinfo/save',0,1),(38,35,'2','信息块管理','信息块管理','fas fa-th-list','blocks','/blocks',3,1),(39,38,'3','查询','信息块-查询','','block:list','/block/list',0,1),(40,38,'3','新增','信息块-新增','','block:add','/block/add',0,1),(42,38,'3','编辑','信息块-编辑','','block:edit','/block/edit',0,1),(43,38,'3','删除','信息块-删除','','block:delete','/block/delete',0,1),(44,38,'3','批量删除','批量删除信息块','','block:batchDelete','/block/batch/delete',0,1),(45,35,'2','推荐位管理','推荐位管理','fas fa-map-marker-alt','positions','/positions',4,1),(46,45,'3','查询','推荐位-查询','','position:list','/position/list',0,1),(47,45,'3','新增','推荐位-新增','','position:add','/position/add',0,1),(48,45,'3','编辑','推荐位-编辑','','position:edit','/position/edit',0,1),(49,45,'3','删除','推荐位-删除','','position:delete','/position/delete',0,1),(50,45,'3','批量删除','推荐位-批量删除','','position:batchDelete','/position/batch/delete',0,1),(52,0,'1','内容管理','文章管理','fas fa-newspaper',NULL,NULL,2,1),(53,52,'2','分类管理','分类管理','fas fa-sitemap','categories','/categories',1,1),(54,53,'3','新增','新增分类','','category:add','/category/add',0,1),(55,53,'3','编辑','编辑分类','','category:edit','/category/edit',0,1),(56,53,'3','删除','删除分类','','category:delete','/category/delete',0,1),(58,53,'3','查询','分类查询','','category:list','/category/list',0,1),(59,52,'2','标签管理','标签管理','fas fa-tag','tags','/tags',2,1),(60,59,'3','查询','查询标签列表','','tag:list','/tag/list',0,1),(61,59,'3','新增','新增标签','','tag:add','/tag/add',0,1),(62,59,'3','编辑','编辑标签','','tag:edit','/tag/edit',0,1),(63,59,'3','删除','删除标签','','tag:delete','/tag/delete',0,1),(64,59,'3','批量删除','批量删除标签','','tag:batchDelete','/tag/batch/delete',0,1),(65,52,'2','文章管理','文章管理','far fa-newspaper','articles','/articles',3,1),(66,65,'3','查询','查询文章','','article:list','/article/list',0,1),(67,65,'3','新增','新增文章','','article:add','/article/add',0,1),(68,65,'3','编辑','编辑文章','','article:edit','/article/edit',0,1),(69,65,'3','删除','删除文章','','article:delete','/article/delete',0,1),(70,65,'3','批量删除','批量删除文章','','article:batchDelete','/article/batch/delete',0,1),(71,52,'2','单页管理','单页管理','far fa-file-alt','single:add','/singles',4,1),(72,35,'2','评论管理','评论管理','fas fa-chess-queen','comments','/comments',5,0),(73,72,'3','查询','查询','','comment:list','/comment/list',0,0),(74,72,'3','审核','审核评论','','comment:audit','/comment/audit',0,0),(75,72,'3','回复','回复评论','','comment:audit','/comment/reply',0,0),(76,72,'3','删除','删除评论','','comment:delete','/comment/delete',0,0),(77,72,'3','批量删除','批量删除评论','','comment:batchDelete','/comment/batch/delete',0,0),(80,79,'3','保存','保存云存储配置','','upload:saveConfig','/upload/saveConfig',0,0),(81,35,'2','主题管理','主题管理','fas fa-th-large','themes','/themes',2,1),(82,81,'3','查询','主题列表','','theme:list','/theme/list',0,1),(83,81,'3','新增','新增主题','','theme:add','/theme/add',0,1),(84,81,'3','启用','启用主题','','theme:use','/theme/use',0,1),(85,81,'3','删除','删除主题','','theme:delete','/theme/delete',0,1),(86,81,'3','批量删除','批量删除主题','','theme:batchDelete','theme/batch/delete',0,1),(87,81,'3','编辑','编辑主题','','theme:edit','/theme/edit',0,1),(92,71,'3','查询','查询单页','','single:list','/single/list',0,1),(93,71,'3','新增','新增单页','','single:add','/single/add',0,1),(94,71,'3','编辑','编辑单页','','single:edit','/single/edit',0,1),(95,71,'3','删除','删除单页','','single:delete','/single/delete',0,1),(96,71,'3','批量删除','批量删除单页','','single:batchDelete','/single/batch/delete',0,1);
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_role`
--

DROP TABLE IF EXISTS `auth_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL COMMENT '角色名称',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否激活',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COMMENT='角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_role`
--

LOCK TABLES `auth_role` WRITE;
/*!40000 ALTER TABLE `auth_role` DISABLE KEYS */;
INSERT INTO `auth_role` (`id`, `role_name`, `role_desc`, `status`) VALUES (2,'超级管理员','超级管理员',1),(3,'新闻编辑','新闻编辑',1),(4,'产品编辑','产品编辑',1);
/*!40000 ALTER TABLE `auth_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_role_permission`
--

DROP TABLE IF EXISTS `auth_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL COMMENT '角色id',
  `perm_id` int(10) unsigned NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb3 COMMENT='角色权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_role_permission`
--

LOCK TABLES `auth_role_permission` WRITE;
/*!40000 ALTER TABLE `auth_role_permission` DISABLE KEYS */;
INSERT INTO `auth_role_permission` (`id`, `role_id`, `perm_id`) VALUES (1,2,1),(2,2,3),(3,2,4),(4,3,7),(5,3,8),(6,2,52),(9,2,65),(10,2,66),(11,2,67),(12,2,68),(13,2,69),(14,2,70),(15,2,88),(16,2,53),(17,2,54),(18,2,55),(19,2,56),(20,2,58),(21,2,59),(22,2,60),(23,2,61),(24,2,62),(25,2,63),(26,2,64),(27,2,35),(28,2,36),(29,2,37),(30,2,81),(31,2,82),(32,2,83),(33,2,84),(34,2,85),(35,2,86),(36,2,87),(37,2,38),(38,2,45),(39,2,46),(40,2,47),(41,2,48),(42,2,49),(43,2,50);
/*!40000 ALTER TABLE `auth_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(40) NOT NULL,
  `salt` varchar(128) DEFAULT NULL COMMENT '加密盐值',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `sex` char(1) DEFAULT NULL COMMENT '性别',
  `age` tinyint(3) unsigned DEFAULT NULL COMMENT '年龄',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `status` tinyint(3) unsigned NOT NULL DEFAULT 1 COMMENT '状态',
  `is_admin` tinyint(4) DEFAULT NULL COMMENT '管理员标志',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `last_login` datetime DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uindex` (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` (`id`, `username`, `password`, `salt`, `nickname`, `email`, `phone`, `sex`, `age`, `avatar`, `status`, `is_admin`, `create_time`, `update_time`, `last_login`) VALUES (1,'admin','fe8a8ee9b08e579485f410d12a7885f8','8d154aa5c111bc4a5f6d24813452fe39','超管','admin@loncol.com','13818006918','1',39,NULL,1,1,NULL,'2022-06-07 14:39:31','2022-06-13 20:21:05');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_role`
--

DROP TABLE IF EXISTS `auth_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户账号',
  `role_id` int(10) unsigned NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='用户角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_role`
--

LOCK TABLES `auth_user_role` WRITE;
/*!40000 ALTER TABLE `auth_user_role` DISABLE KEYS */;
INSERT INTO `auth_user_role` (`id`, `user_id`, `role_id`) VALUES (2,1,3);
/*!40000 ALTER TABLE `auth_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_article`
--

DROP TABLE IF EXISTS `cms_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL COMMENT '栏目类型',
  `article_type` char(1) DEFAULT NULL COMMENT '文章类型',
  `title` varchar(255) DEFAULT NULL COMMENT '文章标题',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `cover` varchar(255) DEFAULT NULL COMMENT '封面',
  `intro` varchar(500) DEFAULT NULL COMMENT '简介',
  `url` varchar(255) DEFAULT '' COMMENT '链接地址',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键字',
  `qrcode` varchar(255) DEFAULT NULL COMMENT '二维码',
  `top_set` tinyint(3) unsigned DEFAULT 255 COMMENT '置顶设置',
  `status` tinyint(3) unsigned DEFAULT NULL COMMENT '状态',
  `publish_date` datetime DEFAULT NULL COMMENT '发布时间',
  `visit_count` int(11) DEFAULT 0 COMMENT '访问量',
  `like_count` int(11) DEFAULT 0 COMMENT '点赞数',
  `comment_count` int(11) DEFAULT 0 COMMENT '评论数',
  `comment_flag` tinyint(4) DEFAULT 0 COMMENT '开启评论',
  `original_flag` tinyint(4) DEFAULT 0 COMMENT '原创标志',
  `recomment_flag` tinyint(4) DEFAULT 0 COMMENT '推荐标志',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user` int(10) unsigned DEFAULT NULL COMMENT '创建者',
  `update_user` int(10) unsigned DEFAULT NULL COMMENT '修改者',
  `deleted` tinyint(4) DEFAULT 0 COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='文章';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_article`
--

LOCK TABLES `cms_article` WRITE;
/*!40000 ALTER TABLE `cms_article` DISABLE KEYS */;
INSERT INTO `cms_article` (`id`, `category_id`, `article_type`, `title`, `author`, `cover`, `intro`, `url`, `keywords`, `qrcode`, `top_set`, `status`, `publish_date`, `visit_count`, `like_count`, `comment_count`, `comment_flag`, `original_flag`, `recomment_flag`, `create_time`, `update_time`, `create_user`, `update_user`, `deleted`) VALUES (1,5,'3','标题test','hardy','http://localhost:8081/upload/20220611/vue_1654914773531.jpg',NULL,'','',NULL,255,1,'2022-05-14 05:19:13',0,0,0,0,0,0,'2022-05-13 14:37:49','2022-05-14 14:37:50',NULL,NULL,0),(2,7,NULL,'Test Front','超管','http://localhost:8081/upload/20220611/kubernetes_1654904162471.jpg',NULL,'','',NULL,255,1,NULL,0,0,0,1,0,1,'2022-05-14 18:28:43','2022-05-14 18:28:43',1,NULL,0);
/*!40000 ALTER TABLE `cms_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_article_content`
--

DROP TABLE IF EXISTS `cms_article_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_article_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL COMMENT '文章编号',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='文章内容';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_article_content`
--

LOCK TABLES `cms_article_content` WRITE;
/*!40000 ALTER TABLE `cms_article_content` DISABLE KEYS */;
INSERT INTO `cms_article_content` (`id`, `article_id`, `content`) VALUES (1,1,'<p>hardy说的话是真的</p>\r\n'),(2,2,'<p>ssssssgggggddddd</p>\r\n');
/*!40000 ALTER TABLE `cms_article_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_article_like`
--

DROP TABLE IF EXISTS `cms_article_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_article_like` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL COMMENT '文章编号',
  `user_id` int(10) unsigned NOT NULL COMMENT '访问者',
  `user_ip` int(10) unsigned NOT NULL COMMENT '访问者IP',
  `like_time` datetime DEFAULT current_timestamp() COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='点赞';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_article_like`
--

LOCK TABLES `cms_article_like` WRITE;
/*!40000 ALTER TABLE `cms_article_like` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_article_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_article_tag`
--

DROP TABLE IF EXISTS `cms_article_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_article_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL COMMENT '文章编号',
  `tag_id` int(10) unsigned NOT NULL COMMENT '标签标号',
  `tag_time` datetime DEFAULT current_timestamp() COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COMMENT='文章标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_article_tag`
--

LOCK TABLES `cms_article_tag` WRITE;
/*!40000 ALTER TABLE `cms_article_tag` DISABLE KEYS */;
INSERT INTO `cms_article_tag` (`id`, `article_id`, `tag_id`, `tag_time`) VALUES (1,1,2,'2022-05-10 23:44:00'),(2,1,3,'2022-05-10 23:44:00'),(3,1,1,'2022-05-10 23:44:00'),(5,2,2,'2022-06-07 11:25:23');
/*!40000 ALTER TABLE `cms_article_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_article_visit`
--

DROP TABLE IF EXISTS `cms_article_visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_article_visit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL COMMENT '文章编号',
  `user_id` int(10) unsigned NOT NULL COMMENT '访问者',
  `user_ip` int(10) unsigned NOT NULL COMMENT '访问者IP',
  `visit_time` datetime DEFAULT current_timestamp() COMMENT '访问时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='文章访问';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_article_visit`
--

LOCK TABLES `cms_article_visit` WRITE;
/*!40000 ALTER TABLE `cms_article_visit` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_article_visit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_category`
--

DROP TABLE IF EXISTS `cms_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT '上级编号',
  `parent_path` varchar(500) DEFAULT NULL COMMENT '上级路径',
  `category_type` char(2) DEFAULT NULL COMMENT '栏目类型',
  `category_name` varchar(100) DEFAULT NULL COMMENT '栏目名称',
  `url` varchar(100) DEFAULT '' COMMENT '链接地址',
  `intro` varchar(500) DEFAULT NULL COMMENT '栏目介绍',
  `icon` varchar(100) DEFAULT NULL COMMENT '栏目图标',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` tinyint(3) unsigned DEFAULT 1 COMMENT '是否激活',
  PRIMARY KEY (`id`),
  KEY `base_category_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COMMENT='栏目';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_category`
--

LOCK TABLES `cms_category` WRITE;
/*!40000 ALTER TABLE `cms_category` DISABLE KEYS */;
INSERT INTO `cms_category` (`id`, `parent_id`, `parent_path`, `category_type`, `category_name`, `url`, `intro`, `icon`, `sort`, `status`) VALUES (1,0,'0,','4','前端技术',NULL,'主要收集、整理的基础前端类文章','fa fa-css3',255,1),(2,0,'0,','2','后端技术','','包括Java、Spring、MySQL、大数据','fa fa-coffee',255,1),(3,0,'0,','1','其他文章',NULL,'记录网站建设以及日常工作、学习中的闲言碎语','fa fa-folder-open-o',255,1),(5,3,'0,3,','3','新闻动态','','新闻动态',NULL,233,1),(6,0,'0,','2','工具资源',NULL,'开发工具、服务端工具、中间件',NULL,255,1),(7,3,'0,3,','3','行业新闻','','可以不',NULL,255,1);
/*!40000 ALTER TABLE `cms_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_category_content`
--

DROP TABLE IF EXISTS `cms_category_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_category_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL COMMENT '栏目编号',
  `title` varchar(100) DEFAULT NULL COMMENT '页面标题',
  `content` text NOT NULL COMMENT '内容',
  `status` tinyint(4) DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='单页';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_category_content`
--

LOCK TABLES `cms_category_content` WRITE;
/*!40000 ALTER TABLE `cms_category_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_category_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_comment`
--

DROP TABLE IF EXISTS `cms_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned DEFAULT NULL COMMENT '被评论的文章Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT '父级评论Id',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '评论者Id',
  `user_ip` int(11) DEFAULT NULL COMMENT '评论时IP',
  `username` varchar(20) DEFAULT NULL COMMENT '评论者账号',
  `nickname` varchar(13) DEFAULT NULL COMMENT '评论者昵称',
  `content` varchar(2000) DEFAULT NULL COMMENT '评论内容',
  `support` tinyint(4) DEFAULT 0 COMMENT '赞',
  `oppose` tinyint(4) DEFAULT 0 COMMENT '踩',
  `qq` varchar(13) DEFAULT NULL COMMENT '评论人QQ',
  `avatar` varchar(255) DEFAULT NULL COMMENT '评论人头像',
  `email` varchar(100) DEFAULT NULL COMMENT '评论人邮箱',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `status` tinyint(1) DEFAULT 0 COMMENT '评论状态',
  `like_count` int(11) DEFAULT 0 COMMENT '点赞数',
  `update_time` datetime DEFAULT current_timestamp() COMMENT '更新时间',
  `create_time` datetime DEFAULT current_timestamp() COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='评论';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_comment`
--

LOCK TABLES `cms_comment` WRITE;
/*!40000 ALTER TABLE `cms_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_info_block`
--

DROP TABLE IF EXISTS `cms_info_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_info_block` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT 0 COMMENT '上级编号',
  `parent_path` varchar(500) DEFAULT '' COMMENT '父路径',
  `position_id` int(10) unsigned DEFAULT NULL COMMENT '推荐位',
  `info_type` char(1) DEFAULT NULL COMMENT '类型',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `intro` varchar(2000) DEFAULT NULL COMMENT '简介',
  `image` varchar(255) DEFAULT '' COMMENT '图片',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '链接',
  `sort` tinyint(3) unsigned DEFAULT 255 COMMENT '排序',
  `status` tinyint(4) DEFAULT NULL COMMENT '是否激活',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_info_block_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='信息块';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_info_block`
--

LOCK TABLES `cms_info_block` WRITE;
/*!40000 ALTER TABLE `cms_info_block` DISABLE KEYS */;
INSERT INTO `cms_info_block` (`id`, `parent_id`, `parent_path`, `position_id`, `info_type`, `title`, `intro`, `image`, `url`, `sort`, `status`) VALUES (1,0,'0,',2,'1','信息块测试','<p>测试以下问题滚滚滚</p>\r\n','http://localhost:8081/upload/20220611/hardy_1654948493317.jpg','222',255,1),(2,1,'0,1',1,'2','友情链接1','<p>杀杀杀</p>\r\n','','',255,1),(3,1,'0,1',1,'1','友情链接2','<p>友情链接</p>\r\n','','http://www.baidu.com',255,1);
/*!40000 ALTER TABLE `cms_info_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_info_position`
--

DROP TABLE IF EXISTS `cms_info_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_info_position` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position_name` varchar(100) NOT NULL COMMENT '推荐位名称',
  `position_desc` varchar(500) DEFAULT NULL COMMENT '推荐位描述',
  `status` tinyint(4) DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_position_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='信息位';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_info_position`
--

LOCK TABLES `cms_info_position` WRITE;
/*!40000 ALTER TABLE `cms_info_position` DISABLE KEYS */;
INSERT INTO `cms_info_position` (`id`, `position_name`, `position_desc`, `status`) VALUES (1,'友情联系','友情联系',1),(2,'首页轮播','首页轮播',1);
/*!40000 ALTER TABLE `cms_info_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_tag`
--

DROP TABLE IF EXISTS `cms_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(100) NOT NULL COMMENT '标签名称',
  `tag_desc` varchar(200) DEFAULT NULL COMMENT '标签描述',
  `status` tinyint(4) DEFAULT 1 COMMENT '是否激活',
  UNIQUE KEY `site_tag_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COMMENT='标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_tag`
--

LOCK TABLES `cms_tag` WRITE;
/*!40000 ALTER TABLE `cms_tag` DISABLE KEYS */;
INSERT INTO `cms_tag` (`id`, `tag_name`, `tag_desc`, `status`) VALUES (1,'python','python 版本',1),(2,'java','java',1),(3,'spring','spring',1),(4,'vue','vue学习',1);
/*!40000 ALTER TABLE `cms_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_config`
--

DROP TABLE IF EXISTS `sys_dict_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `config_code` varchar(50) DEFAULT NULL COMMENT '配置代码',
  `config_name` varchar(255) DEFAULT NULL COMMENT '配置名称',
  `data_from` varchar(10) DEFAULT NULL COMMENT '数据来源',
  `data_define` varchar(255) DEFAULT NULL COMMENT '数据定义',
  `status` tinyint(3) unsigned DEFAULT 1 COMMENT '是否激活',
  UNIQUE KEY `sys_dict_config_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COMMENT='字典配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_config`
--

LOCK TABLES `sys_dict_config` WRITE;
/*!40000 ALTER TABLE `sys_dict_config` DISABLE KEYS */;
INSERT INTO `sys_dict_config` (`id`, `config_code`, `config_name`, `data_from`, `data_define`, `status`) VALUES (1,'bool','是否编码','JSON','[{\"code\":\"1\", \"name\":\"是\"},{\"code\":\"2\", \"name\":\"否\"}]',1),(2,'sex','性别编码','JSON','[{\"code\":\"1\", \"name\":\"男\"},{\"code\":\"2\", \"name\":\"女\"}]',1),(3,'permission_type','权限类型','SQL','SELECT code,name FROM sys_dict_data WHERE type_id=3',1),(4,'category_type','栏目类型','SQL','SELECT code,name FROM sys_dict_data WHERE type_id=4',1),(5,'article_type','文章类型','SQL','SELECT code,name FROM sys_dict_data WHERE type_id=5',1),(6,'article_status','文章状态','SQL','SELECT code,name FROM sys_dict_data WHERE type_id=6',1),(7,'notice_type','公告类型','SQL','SELECT code,name FROM sys_dict_data WHERE type_id=7',1),(8,'block_type','信息块类型','SQL','SELECT code,name FROM sys_dict_data WHERE type_id=8',1);
/*!40000 ALTER TABLE `sys_dict_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_data`
--

DROP TABLE IF EXISTS `sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type_id` smallint(5) unsigned DEFAULT 0 COMMENT '类型编号',
  `parent_code` varchar(50) DEFAULT '0' COMMENT '上级代码',
  `code` varchar(50) DEFAULT NULL COMMENT '字典代码',
  `name` varchar(255) DEFAULT NULL COMMENT '字典名称',
  `status` tinyint(3) unsigned DEFAULT 1 COMMENT '是否激活',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_dict_data_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb3 COMMENT='字典数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_data`
--

LOCK TABLES `sys_dict_data` WRITE;
/*!40000 ALTER TABLE `sys_dict_data` DISABLE KEYS */;
INSERT INTO `sys_dict_data` (`id`, `type_id`, `parent_code`, `code`, `name`, `status`) VALUES (1,1,'0','1','是',1),(2,1,'0','2','否',1),(3,2,'0','1','男',1),(4,2,'0','2','女',1),(5,3,'0','1','目录',1),(6,3,'0','2','菜单',1),(7,3,'0','3','按钮',1),(8,4,'0','1','目录',1),(9,4,'0','2','单页',1),(10,4,'0','3','文章',1),(11,4,'0','4','链接',1),(12,4,'0','5','产品',0),(13,4,'0','6','待用',0),(14,5,'0','1','单页',1),(15,5,'0','3','新闻',1),(16,5,'0','4','公告',1),(17,5,'0','5','产品',1),(18,6,'0','0','草稿',1),(19,6,'0','1','发布',1),(20,6,'0','2','撤回',1),(21,6,'0','9','归档',1),(22,7,'0','1','通知',1),(23,7,'0','2','公告',1),(24,8,'0','1','文本',1),(25,8,'0','2','图片',1),(26,8,'0','3','视频',1),(27,8,'0','4','音频',1),(28,8,'0','5','链接',1),(29,8,'0','6','按钮',1),(30,4,'0','7','待用',0),(31,9,'0','0','待审核',1),(32,9,'0','1','审核通过',1),(33,9,'0','2','审核失败',1);
/*!40000 ALTER TABLE `sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_type`
--

DROP TABLE IF EXISTS `sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type_id` smallint(5) unsigned DEFAULT 0 COMMENT '类型编号',
  `type_name` varchar(100) DEFAULT NULL COMMENT '类型名称',
  `status` tinyint(3) unsigned DEFAULT 1 COMMENT '是否激活',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_dict_type_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COMMENT='字典类型';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_type`
--

LOCK TABLES `sys_dict_type` WRITE;
/*!40000 ALTER TABLE `sys_dict_type` DISABLE KEYS */;
INSERT INTO `sys_dict_type` (`id`, `type_id`, `type_name`, `status`) VALUES (1,1,'是否',1),(2,2,'性别',1),(3,3,'权限类型',1),(4,4,'栏目类型',1),(5,5,'文章类型',1),(6,6,'文章状态',1),(7,7,'公告类型',1),(8,8,'信息块类型',1),(9,9,'评论状态',1);
/*!40000 ALTER TABLE `sys_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_site_config`
--

DROP TABLE IF EXISTS `sys_site_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_site_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_key` varchar(50) DEFAULT NULL COMMENT '键',
  `site_val` varchar(2000) DEFAULT NULL COMMENT '值',
  `site_desc` varchar(500) DEFAULT NULL COMMENT '描述',
  `status` tinyint(4) DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_config_id_uindex` (`id`),
  UNIQUE KEY `key` (`site_key`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3 COMMENT='网站配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_site_config`
--

LOCK TABLES `sys_site_config` WRITE;
/*!40000 ALTER TABLE `sys_site_config` DISABLE KEYS */;
INSERT INTO `sys_site_config` (`id`, `site_key`, `site_val`, `site_desc`, `status`) VALUES (1,'SITE_NAME','上海程序员','1',1),(2,'SITE_KWD','Java Spring SpringBoot SpringCloud Vue','1',1),(3,'SITE_DESC','个人博客网站，技术交流，经验分享。','1',1),(4,'SITE_PERSON_PIC','http://www.loncol.com/wp-content/uploads/pic/hardy.jpg','1',1),(5,'SITE_PERSON_NAME','Hardy1','1',1),(6,'SITE_PERSON_DESC','上海程序员，把热爱当作事业。','1',1),(7,'BAIDU_PUSH_URL','http://data.zz.baidu.com/urls?site=https://www.puboot.com&token=EjnfUGGJ1drKk4Oy','1',1),(11,'SITE_STATIC','off',NULL,1);
/*!40000 ALTER TABLE `sys_site_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_site_stat`
--

DROP TABLE IF EXISTS `sys_site_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_site_stat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_count` int(11) NOT NULL DEFAULT 0 COMMENT '栏目数',
  `article_count` int(11) NOT NULL DEFAULT 0 COMMENT '文章数',
  `tag_count` int(11) NOT NULL DEFAULT 0 COMMENT '标签数',
  `visit_count` int(11) NOT NULL DEFAULT 0 COMMENT '浏览量',
  `like_count` int(11) NOT NULL DEFAULT 0 COMMENT '点赞量',
  `comment_count` int(11) NOT NULL DEFAULT 0 COMMENT '评论量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='网站统计';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_site_stat`
--

LOCK TABLES `sys_site_stat` WRITE;
/*!40000 ALTER TABLE `sys_site_stat` DISABLE KEYS */;
INSERT INTO `sys_site_stat` (`id`, `category_count`, `article_count`, `tag_count`, `visit_count`, `like_count`, `comment_count`) VALUES (1,23,1112,5,3678,22,222);
/*!40000 ALTER TABLE `sys_site_stat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_site_theme`
--

DROP TABLE IF EXISTS `sys_site_theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_site_theme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme_name` varchar(50) DEFAULT NULL COMMENT '主题名',
  `theme_desc` varchar(255) DEFAULT NULL COMMENT '主题描述',
  `theme_icon` varchar(255) DEFAULT NULL COMMENT '主题预览图',
  `status` tinyint(3) unsigned DEFAULT NULL COMMENT '是否激活',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='系统主题';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_site_theme`
--

LOCK TABLES `sys_site_theme` WRITE;
/*!40000 ALTER TABLE `sys_site_theme` DISABLE KEYS */;
INSERT INTO `sys_site_theme` (`id`, `theme_name`, `theme_desc`, `theme_icon`, `status`) VALUES (1,'pblog','默认主题','',1);
/*!40000 ALTER TABLE `sys_site_theme` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-13 20:36:44
