# MP条件构造器



| 条件操作符 | 备注               |
| ---------- | ------------------ |
| exact      | 等于，不区分大小写 |
| iexact     | 等于，区分大小写   |
| contains   | 包含，不区分大小写 |
| icontains  | 包含，区分大小写 |
| gt         | 大于 |
| gte        | 大于等于 |
| lt         | 小于 |
| lte         | 小于等于 |
| in         | 在其中 |
| nin         | 不在其中 |
| startswith | 开始于，不区分大小写 |
| istartswith | 开始于，区分大小写 |
| endswith | 结束于，不区分大小写 |
| iendswith | 结束于，区分大小写 |
| range    | 在范围内，不区分大小写 |
| norange  | 不在范围内 |
| unequal         | 不等于 |
| sorting         | 排序字段 |
