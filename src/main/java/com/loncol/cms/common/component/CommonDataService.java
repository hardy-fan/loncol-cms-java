package com.loncol.cms.common.component;

import cn.hutool.core.util.StrUtil;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.po.Category;
import com.loncol.cms.service.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 公共数据服务类
 * 生成数据对象供前端thymeleaf调用
 * @author Hardy
 * @date 2022-05-05
 */
@Slf4j
@Component("commonDataService")
@AllArgsConstructor
public class CommonDataService {

    private final ArticleService articleService;
    private final CategoryService categoryService;
    private final TagService tagService;
    private final SiteStatService siteStatService;
    private final SiteConfigService siteConfigService;

    public Map<String, Object> getAllCommonData() {
        Map<String, Object> result = new HashMap<>(DataTypeEnum.values().length);
        for (DataTypeEnum dataTypeEnum : DataTypeEnum.values()) {
            result.put(dataTypeEnum.name(), get(dataTypeEnum.name()));
        }
        return result;
    }

    /**
     * 根据模块名称获得数据对象
     * @param moduleName
     * @return
     */
    private Object get(String moduleName) {
        try {
            DataTypeEnum dataTypeEnum = DataTypeEnum.valueOf(moduleName);
            switch (dataTypeEnum) {
                case CATEGORY_LIST:
                    Category category = new Category();
                    category.setStatus(CoreConst.STATUS_VALID);
                    return categoryService.list();
                case TAG_LIST:
                    return tagService.list();
                case SLIDER_LIST:
                    return articleService.sliderList();
                case RECENT_LIST:
                    return articleService.recentList(CoreConst.PAGE_SIZE);
                case RECOMMENDED_LIST:
                    return articleService.recommendedList(CoreConst.PAGE_SIZE);
                case HOT_LIST:
                    return articleService.hotList(CoreConst.PAGE_SIZE);
                case RANDOM_LIST:
                    return articleService.randomList(CoreConst.PAGE_SIZE);
                case SITE_INFO:
                    return siteStatService.getSiteInfo();
                case SITE_CONFIG:
                    return siteConfigService.selectAll();
                default:
                    return StrUtil.EMPTY;
            }
        } catch (Exception e) {
            log.error("获取网站公共信息[{}]发生异常: {}", moduleName, e.getMessage(), e);
        }
        return StrUtil.EMPTY;
    }

    /**
     * 数据类型
     */
    private enum DataTypeEnum {
        /**
         * 分类
         */
        CATEGORY_LIST,
        /**
         * 标签
         */
        TAG_LIST,
        /**
         * 轮播文章
         */
        SLIDER_LIST,
        /**
         * 最近文章
         */
        RECENT_LIST,
        /**
         * 推荐文章
         */
        RECOMMENDED_LIST,
        /**
         * 热门文章
         */
        HOT_LIST,
        /**
         * 随机文章
         */
        RANDOM_LIST,
        /**
         * 网站统计
         */
        SITE_INFO,
        /**
         * 网站配置
         */
        SITE_CONFIG
    }
}
