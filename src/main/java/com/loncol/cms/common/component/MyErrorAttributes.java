package com.loncol.cms.common.component;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

/**
 * 自定义错误属性
 * @author Hardy
 * @date 2022/05/01
 */
@Component
public class MyErrorAttributes extends DefaultErrorAttributes {
    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
        Map<String, Object> map = super.getErrorAttributes(webRequest, options);

        Object attribute = webRequest.getAttribute("ext", 0);
        if (attribute instanceof Map) {
            Map<String, Object> ext = (Map<String, Object>) attribute;
            map.put("ext", ext);
        }
        return map;
    }
}
