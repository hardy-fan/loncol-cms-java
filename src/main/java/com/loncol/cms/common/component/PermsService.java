package com.loncol.cms.common.component;

import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Component;

/**
 * 权限服务
 * 实现前端按钮权限，js调用thymeleaf
 * @author Hardy
 * @date 2022-05-05
 */
@Component("perms")
public class PermsService {
    public boolean hasPerm(String permission) {
        return SecurityUtils.getSubject().isPermitted(permission);
    }
}
