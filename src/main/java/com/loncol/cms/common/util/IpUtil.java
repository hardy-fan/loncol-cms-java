package com.loncol.cms.common.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

/**
 * ip工具类
 * 从请求中获取IP地址
 * @author Hardy
 * @date 2022-05-05
 */
@Slf4j
@UtilityClass
public class IpUtil {

    /**
     * 从HttpServletRequest中获取Ip字符串
     * @param request 请求
     * @return ip字符串
     */
    public static String getIpStr(HttpServletRequest request) {
        String ip = null;
        String UNKNOWN = "unknown";
        Integer IP_MAXLEN = 15;
        Character SPLIT_CHAR = ',';

        String[] texts = new String[]{
                "X-Real-IP",
                "x-forwarded-for",
                "Proxy-Client-IP",
                "WL-Proxy-Client-IP",
                "HTTP_CLIENT_IP",
                "HTTP_X_FORWARDED_FOR"
        };

        for (String text:texts){
            ip = request.getHeader(text);
            if(ip.length() > 0 && ip.contains(".")){
                break;
            }
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        if (ip != null && ip.length() > IP_MAXLEN) {
            if (ip.indexOf(SPLIT_CHAR) > 0) {
                ip = ip.substring(0, ip.indexOf(','));
            }
        }
        return ip;
    }
}
