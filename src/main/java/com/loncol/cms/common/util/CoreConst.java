package com.loncol.cms.common.util;

import lombok.experimental.UtilityClass;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 常量工具类
 * @author Hardy
 * @date 2022-05-05
 */
@UtilityClass
public class CoreConst {
    public static final Integer SUCCESS_CODE = 200;
    public static final Integer FAIL_CODE = 500;
    public static final Integer STATUS_VALID = 1;
    public static final Integer STATUS_INVALID = 0;
    public static final Integer PAGE_SIZE = 10;
    public static final Integer TOP_MENU_ID = 0;
    public static final String TOP_MENU_NAME = "顶层菜单";
    public static final String SHIRO_REDIS_SESSION_PREFIX = "loncol_cms:session:";
    public static final String SHIRO_REDIS_CACHE_NAME = "shiro_loncol_cms";

    public static final String ADMIN_PREFIX = "admin/";
    public static final String THEME_PREFIX = "theme/";
    public static final String SITE_STATIC_KEY = "SITE_STATIC";
    public static final AtomicBoolean SITE_STATIC = new AtomicBoolean(false);
}
