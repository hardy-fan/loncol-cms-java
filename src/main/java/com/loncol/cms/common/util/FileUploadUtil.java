package com.loncol.cms.common.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 文件上传工具类
 * @author Hardy
 * @date 2022-05-05
 */
@Slf4j
@UtilityClass
public class FileUploadUtil {
    public String uploadLocal(MultipartFile mf, String uploadPath) {
        String res = "";
        try {
            String nowdayStr = new SimpleDateFormat("yyyyMMdd").format(new Date());
            String uploadFullPath = uploadPath + File.separator + nowdayStr + File.separator;
            File fileDir = new File(uploadFullPath);
            if (!fileDir.exists() && !fileDir.mkdirs()) {
                log.error("创建文件夹失败: {}", uploadFullPath);
                return null;
            }
            String orgName = mf.getOriginalFilename();
            String fileName = orgName.substring(0, orgName.lastIndexOf('.')) + '_' + System.currentTimeMillis() + orgName.substring(orgName.indexOf('.'));

            String savePath = fileDir.getPath() + File.separator + fileName;
            File saveFile = new File(savePath);
            FileCopyUtils.copy(mf.getBytes(), saveFile);
            res = nowdayStr + File.separator + fileName;
            if (res.contains("\\")) {
                res = res.replaceAll("\\\\", "/");
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return res;
    }
}
