package com.loncol.cms.common.exception;


/**
 * 自定义运行时异常
 *
 * @author Hardy
 * @date 2022-05-05
 */
public class AppException extends RuntimeException {

    private static final long serialVersionUID = 4018029437309683073L;

    public AppException() {
        super();
    }

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }
}
