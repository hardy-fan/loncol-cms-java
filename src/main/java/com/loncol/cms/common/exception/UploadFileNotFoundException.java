package com.loncol.cms.common.exception;

/**
 * <p>
 * 上传文件未找到异常
 * </p>
 * @author Hardy
 * @date 2022-05-05
 */
public class UploadFileNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -7812372861340782170L;

    public UploadFileNotFoundException(String message) {
        super(message);
    }

}
