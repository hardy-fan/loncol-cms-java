package com.loncol.cms.common.shiro;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.po.User;
import com.loncol.cms.service.PermissionService;
import com.loncol.cms.service.RoleService;
import com.loncol.cms.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.apache.shiro.util.ByteSource;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 自定义shiro realm，进行鉴权和认证
 *
 * @author Hardy
 * @date 2022-05-05
 */
@Component
public class MyShiroRealm extends AuthorizingRealm {

    @Lazy @Autowired
    private UserService userService;
    @Lazy @Autowired
    private RoleService roleService;
    @Lazy @Autowired
    private PermissionService permissionService;
    @Lazy @Autowired
    private RedisSessionDAO redisSessionDAO;

    /**
     * 鉴权
     * @param principals 验证主体
     * @return 认证信息
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        if (principals == null) {
            throw new AuthorizationException("principals should not be null");
        }
        User user = (User) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        // 将admin账号的编号暂时设置为0是为了后续进行权限过滤
        Integer userId = "admin".equals(user.getUsername())? 0: user.getId();
        info.setRoles(Convert.toSet(String.class, roleService.findRoleIdsByUserId(userId)));
        info.setStringPermissions(permissionService.findMarksByUserId(userId));
        return info;
    }

    /**
     * 认证
     * @param token token
     * @return 认证信息
     * @throws AuthenticationException 异常
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) {
        String username = (String) token.getPrincipal();
        User user = userService.getOne(new QueryWrapper<User>().eq("username", username));
        if (user == null) {
            throw new UnknownAccountException();
        }
        if (CoreConst.STATUS_INVALID.equals(user.getStatus())) {
            throw new LockedAccountException();
        }
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes()))
                .getRequest();

        return new SimpleAuthenticationInfo(
                user,
                user.getPassword(),
                ByteSource.Util.bytes(user.getCredentialsSalt()),
                getName()
        );
    }

    /**
     * 清除认证信息
     *
     * @param usernames 待清除认证信息的username列表
     */
    public void removeCachedAuthenticationInfo(List<String> usernames) {
        if (CollUtil.isNotEmpty(usernames)) {
            Set<SimplePrincipalCollection> set = getSpcListByUserIds(usernames);
            RealmSecurityManager securityManager = (RealmSecurityManager) SecurityUtils.getSecurityManager();
            MyShiroRealm realm = (MyShiroRealm) securityManager.getRealms().iterator().next();
            for (SimplePrincipalCollection simplePrincipalCollection : set) {
                realm.clearCachedAuthenticationInfo(simplePrincipalCollection);
            }
        }
    }

    /**
     * 清除当前session存在的用户的权限缓存
     *
     * @param userIds 已经修改了权限的userIds列表
     */
    public void clearAuthorizationByUserId(List<String> userIds) {
        if (CollUtil.isNotEmpty(userIds)) {
            Set<SimplePrincipalCollection> set = getSpcListByUserIds(userIds);
            RealmSecurityManager securityManager = (RealmSecurityManager) SecurityUtils.getSecurityManager();
            MyShiroRealm realm = (MyShiroRealm) securityManager.getRealms().iterator().next();
            for (SimplePrincipalCollection simplePrincipalCollection : set) {
                realm.clearCachedAuthorizationInfo(simplePrincipalCollection);
            }
        }

    }

    /**
     * 根据用户id获取所有spc
     *
     * @param userIds 已经修改了权限的userId
     * @return 主题集合
     */
    private Set<SimplePrincipalCollection> getSpcListByUserIds(List<String> userIds) {
        Collection<Session> sessions = redisSessionDAO.getActiveSessions();
        Set<SimplePrincipalCollection> set = new HashSet<>();
        for (Session session : sessions) {
            Object obj = session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
            if (obj instanceof SimplePrincipalCollection) {
                SimplePrincipalCollection spc = (SimplePrincipalCollection) obj;
                obj = spc.getPrimaryPrincipal();
                if (obj instanceof User) {
                    User user = (User) obj;
                    if (userIds.contains(user.getId())) {
                        set.add(spc);
                    }
                }
            }
        }
        return set;
    }

    @Override
    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
        hashedCredentialsMatcher.setHashIterations(2);
        super.setCredentialsMatcher(hashedCredentialsMatcher);
    }
}
