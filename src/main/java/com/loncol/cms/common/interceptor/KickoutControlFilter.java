package com.loncol.cms.common.interceptor;

import com.alibaba.fastjson.JSON;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.po.User;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * 访问控制过滤器
 * 踢人
 * @author Hardy
 * @date 2022-05-05
 */
@Slf4j
@Setter
public class KickoutControlFilter extends AccessControlFilter {
    /**
     * 踢出后到的地址
     */
    private String kickoutUrl;

    /**
     * 用户session里的属性：是否应该被踢出
     */
    private String kickoutSessionAttrName = "kickout";
    /**
     * 踢出
     * True：踢出之后登录的用户
     * False：踢出之前登录的用户
     */
    private boolean kickoutAfter;
    /**
     * 同一个帐号最大会话数 默认5
     */
    private int maxSession = 5;

    private SessionManager sessionManager;

    private Cache<String, Deque<Serializable>> cache;


    /**
     * 设置Cache的key的前缀
     * @param cacheManager
     */
    public void setCacheManager(CacheManager cacheManager) {
        this.cache = cacheManager.getCache(CoreConst.SHIRO_REDIS_CACHE_NAME);
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        Subject subject = getSubject(request, response);
        if (!subject.isAuthenticated() && !subject.isRemembered()) {
            return true;
        }

        Session session = subject.getSession();
        User user = (User) subject.getPrincipal();
        String username = user.getUsername();
        Serializable sessionId = session.getId();

        Deque<Serializable> deque = cache.get(username);

        if (deque == null) {
            deque = new LinkedList<Serializable>();
        }
        if (!deque.contains(sessionId) && session.getAttribute(kickoutSessionAttrName) == null) {
            deque.push(sessionId);
            cache.put(username, deque);
        }

        while (deque.size() > maxSession) {
            Serializable kickoutSessionId = kickoutAfter ? deque.removeFirst() : deque.removeLast();
            cache.put(username, deque);

            Session kickoutSession = sessionManager.getSession(new DefaultSessionKey(kickoutSessionId));
            if (kickoutSession != null) {
                kickoutSession.setAttribute(kickoutSessionAttrName, true);
            }
        }
        if (session.getAttribute(kickoutSessionAttrName) != null
                && (Boolean) session.getAttribute(kickoutSessionAttrName)) {
            subject.logout();
            saveRequest(request);

            Map<String, String> resultMap = new HashMap<String, String>(5);
            if ("XMLHttpRequest".equalsIgnoreCase(((HttpServletRequest) request).getHeader("X-Requested-With"))) {
                resultMap.put("user_status", "300");
                resultMap.put("message", "您已经在其他地方登录，请重新登录！");
                out(response, resultMap);
            } else {
                WebUtils.issueRedirect(request,
                        response, kickoutUrl,
                        null,
                        true,
                        false);
            }
            return false;
        }
        return true;
    }

    private static void out(ServletResponse response, Map<String, String> resultMap) throws IOException {
        response.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter();) {
            out.println(JSON.toJSONString(resultMap));
            out.flush();
        } catch (Exception e) {
            log.error("输出返回JSON发生异常:{}", e.getMessage(), e);
        }
    }
}
