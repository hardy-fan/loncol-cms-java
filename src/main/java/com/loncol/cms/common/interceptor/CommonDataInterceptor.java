package com.loncol.cms.common.interceptor;

import com.loncol.cms.common.component.CommonDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 公共数据拦截器
 * @author Hardy
 * @date 2022-06-13
 */
@Component
@AllArgsConstructor
public class CommonDataInterceptor implements HandlerInterceptor {

    private final CommonDataService commonDataService;

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object handler,
                           ModelAndView mv) throws Exception {
        if (mv != null) {
            mv.addAllObjects(commonDataService.getAllCommonData());
        }
    }
}