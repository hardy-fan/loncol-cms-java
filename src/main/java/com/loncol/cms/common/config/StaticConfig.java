package com.loncol.cms.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 静态资源属性
 * 从配置文件中获取配置信息
 * @author hardy
 * @date 2022-05-02
 */
@Data
@Component
@ConfigurationProperties(prefix = "static")
public class StaticConfig {
    private String accessPathPattern = "/html/**";
    private String folder;
}
