package com.loncol.cms.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 文件上传属性
 * 从配置文件中获取配置信息
 * @author Hardy
 * @date 2022-05-05
 */

@Data
@Component
@ConfigurationProperties(prefix = "file")
public class FileConfig {
    private String accessPathPattern = "/upload/**";
    private String uploadFolder;
    private String accessPrefixUrl;
}
