package com.loncol.cms.common.config;

import cn.hutool.core.util.StrUtil;
import com.loncol.cms.common.interceptor.CommonDataInterceptor;
import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

/**
 * TODO：与业务有关
 *
 * @author Hardy
 * @date 2022/05/01
 */
@Configuration
@AllArgsConstructor
@EnableConfigurationProperties({FileConfig.class, StaticConfig.class})
public class WebMvcConfig implements WebMvcConfigurer {

    private final FileConfig fileConfig;
    private final StaticConfig staticConfig;
    private final CommonDataInterceptor commonDataInterceptor;

    /**
     * 配置本地文件上传的虚拟路径和静态化的文件生成路径
     * 这是一种图片上传访问图片的方法，实际上也可以使用nginx反向代理访问图片
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 文件上传
        String uploadFolder = fileConfig.getUploadFolder();
        uploadFolder = StrUtil.appendIfMissing(uploadFolder, File.separator);
        registry.addResourceHandler(fileConfig.getAccessPathPattern())
                .addResourceLocations("file:" + uploadFolder);
        // 静态化
        String staticFolder = staticConfig.getFolder();
        staticFolder = StrUtil.appendIfMissing(staticFolder, File.separator);
        registry.addResourceHandler(staticConfig.getAccessPathPattern())
                .addResourceLocations("file:" + staticFolder);
    }

    /**
     * 添加拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(commonDataInterceptor).addPathPatterns("/**");
    }
}
