/**
 *  SpringBoot启动类
 * @author Hardy
 * @date 2022-05-03
 */
package com.loncol.cms;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@SpringBootApplication
@MapperScan("com.loncol.cms.mapper")
@EnableConfigurationProperties
public class WebApplication {

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(WebApplication.class, args);

        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        log.info("\n----------------------------------------------------------" +
                "\nApplication is running! " +
                "\nhttp://" + ip + ":" + port +
                "\nhttp://" + ip + ":" + port + "/admin" +
                "\n----------------------------------------------------------");
    }
}
