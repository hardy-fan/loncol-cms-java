package com.loncol.cms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.loncol.cms.model.po.Article;
import com.loncol.cms.model.vo.ArticleConditionVo;

import java.io.Serializable;
import java.util.List;

/**
 * 文章服务
* @author Hardy
* @date 2022-05-08
*/
public interface ArticleService extends IService<Article> {

    /**
     * 分页查询
     *
     * @param page
     * @param vo
     * @return
     *
     */
    List<Article> findByCondition(IPage<Article> page, ArticleConditionVo vo);

    /**
     * 轮播
     * @return
     */
    List<Article> sliderList();
    /**
     * 站长推荐
     *
     * @param pageSize
     * @return
     */
    List<Article> recommendedList(int pageSize);

    /**
     * 最近文章
     *
     * @param pageSize
     * @return
     */

    List<Article> recentList(int pageSize);

    /**
     * 随机文章
     *
     * @param pageSize
     * @return
     */
    List<Article> randomList(int pageSize);

    /**
     * 热门文章
     *
     * @param pageSize
     * @return
     */
    List<Article> hotList(int pageSize);

    /**
     * 分类文章
     *
     * @param categoryId
     * @return
     */
    List<Article> selectByCategoryId(Integer categoryId);

    /**
     * 获得文章详细信息
     * @param id
     * @return
     */
    Article getDetailById(Serializable id);

    /**
     * 设置文章标签
     * @param articleId
     * @param tagIds
     */
    void addAssignTags(Integer articleId, List<Integer> tagIds);

    /**
     * 设置文章内容
     * @param articleId
     * @param content
     */
    void assignContent(Integer articleId, String content);

}
