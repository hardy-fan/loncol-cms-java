package com.loncol.cms.service;

import com.loncol.cms.model.po.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Hardy
* @description 针对表【auth_role_permission(角色权限)】的数据库操作Service
* @createDate 2022-05-08 08:12:08
*/
public interface RolePermissionService extends IService<RolePermission> {
}
