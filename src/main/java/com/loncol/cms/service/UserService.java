package com.loncol.cms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.loncol.cms.model.po.User;
import com.loncol.cms.model.vo.UserOnlineVo;

import java.io.Serializable;
import java.util.List;

/**
 * 用户服务
 * @author Hardy
 * @date 2022-10-30
*/
public interface UserService extends IService<User> {

    /**
     * 根据查询条件获得分页列表数据
     * @param userQuery 查询条件对象
     * @param pageNum 页码
     * @param pageSize 分页设置
     * @return
     */
    IPage<User> listByCondition(User userQuery, Integer pageNum, Integer pageSize);

    /**
     * 根据用户账号获得用户对象
     * @param username
     * @return
     */
    User selectByUsername(String username);

    /**
     * 设置用户状态
     * @param id
     * @param status
     * @return
     */
    boolean updateStatus(Integer id, Integer status);

    /**
     * 批量设置用户状态
     * @param ids
     * @param status
     * @return
     */
    boolean updateStatusBatch(List<Integer> ids, Integer status);

    /**
     * 为用户设置角色列表
     * @param userId
     * @param roleIds
     */
    void addAssignRole(Integer userId, List<Integer> roleIds);

    /**
     * 更新用户登录时间
     * @param user
     */
    void updateLastLogin(User user);

    /**
     * 查询在线用户
     * @param userOnlineVo
     * @return
     */
    List<UserOnlineVo> selectOnlineUsers(UserOnlineVo userOnlineVo);

    /**
     * 踢出用户
     * @param sessionId
     * @param username
     */
    void kickout(Serializable sessionId, String username);

    /**
     * 设置用户密码加密
     * 密码尚未加密
     *
     * @param user
     */
    void encryptPassword(User user);

    /**
     * 获得用户加密后的密码
     * 密码尚未加密
     *
     * @param user
     * @return
     */
    String getEncryptPassword(User user);

}
