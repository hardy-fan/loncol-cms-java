package com.loncol.cms.service;

import com.loncol.cms.model.po.Category;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 栏目服务
* @author Hardy
* @date 2022-05-08
*/
public interface CategoryService extends IService<Category> {
    /**
     * 根据条件获取栏目列表
     * @param categoryCondition
     * @return
     */
    List<Category> selectCategories(Category categoryCondition);

    /**
     * 根据父栏目获得栏目列表
     * @param pid
     * @return
     */
    List<Category> selectByPid(Integer pid);

    /**
     * 根据父栏目获得栏目列表
     * @param pids
     * @return
     */
    List<Category> selectByPids(Integer[] pids);

    /**
     * 根据栏目类型获得栏目列表
     * @param typeId
     * @return
     */
    List<Category> selectByTypeId(Integer typeId);

    /**
     * 获取栏目详情
     * @param id
     * @return
     */
    Category getDetailById(Integer id);
}
