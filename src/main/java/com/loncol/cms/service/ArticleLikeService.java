package com.loncol.cms.service;

import com.loncol.cms.model.po.ArticleLike;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 点赞服务
* @author Hardy
* @date 2022-05-09
*/
public interface ArticleLikeService extends IService<ArticleLike> {
}
