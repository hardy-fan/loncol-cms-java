package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.SiteStat;
import com.loncol.cms.service.SiteStatService;
import com.loncol.cms.mapper.SiteStatMapper;
import com.loncol.cms.model.vo.StatisticVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 网站统计服务实现
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class SiteStatServiceImpl extends ServiceImpl<SiteStatMapper, SiteStat>
    implements SiteStatService{

    private final SiteStatMapper siteStatMapper;
    @Override
    public Map<String, Object> getSiteInfo() {
        return siteStatMapper.selectMaps(null).get(0);
    }

    @Override
    public StatisticVo indexStatistic() {
        Map<String, Object> siteStat = getSiteInfo();

        Map<String, Integer> lookCounts = new HashMap<String, Integer>() ;
        lookCounts.put("2022-05-08", 79);
        lookCounts.put("2022-05-09", 82);
        lookCounts.put("2022-05-10", 86);
        lookCounts.put("2022-05-11", 90);
        lookCounts.put("2022-05-12", 86);

        Map<String, Integer> userCounts = new HashMap<String, Integer>() ;
        userCounts.put("2022-05-08", 56);
        userCounts.put("2022-05-09", 43);
        userCounts.put("2022-05-10", 56);
        userCounts.put("2022-05-11", 78);
        userCounts.put("2022-05-12", 33);


        return StatisticVo.builder()
                .articleCount((int)siteStat.get("article_count"))
                .commentCount((int)siteStat.get("comment_count"))
                .visitCount((int)siteStat.get("visit_count"))
                .userCount(0)
                .lookCountByDay(lookCounts)
                .userCountByDay(userCounts)
                .build();
    }
}




