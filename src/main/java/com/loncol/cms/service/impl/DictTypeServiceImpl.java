package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.DictType;
import com.loncol.cms.service.DictTypeService;
import com.loncol.cms.mapper.DictTypeMapper;
import org.springframework.stereotype.Service;

/**
 * 字典类型服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
public class DictTypeServiceImpl extends ServiceImpl<DictTypeMapper, DictType>
    implements DictTypeService{

}




