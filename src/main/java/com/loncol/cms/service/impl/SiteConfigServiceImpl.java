package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.mapper.SiteConfigMapper;
import com.loncol.cms.model.po.SiteConfig;
import com.loncol.cms.service.SiteConfigService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 网站配置服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class SiteConfigServiceImpl extends ServiceImpl<SiteConfigMapper, SiteConfig>
    implements SiteConfigService{

    private final SiteConfigMapper configMapper;

    @PostConstruct
    public void init() {
        CoreConst.SITE_STATIC.set("on".equalsIgnoreCase(
                selectAll().getOrDefault(CoreConst.SITE_STATIC_KEY, "false")));
    }

    @Override
    public Map<String, String> selectAll() {
        List<SiteConfig> sysConfigs = configMapper.selectList(Wrappers.emptyWrapper());
        Map<String, String> map = new HashMap<>(sysConfigs.size());
        for (SiteConfig config : sysConfigs) {
            map.put(config.getSiteKey(), config.getSiteVal());
        }
        return map;
    }

    @Override
    @CacheEvict(value = "site", key = "'config'", allEntries = true)
    public boolean updateByKey(String key, String value) {
        if (getOne(Wrappers.<SiteConfig>lambdaQuery().eq(SiteConfig::getSiteKey, key)) != null) {
            return update(Wrappers.<SiteConfig>lambdaUpdate()
                    .eq(SiteConfig::getSiteKey, key)
                    .set(SiteConfig::getSiteVal, value));
        } else {
            return save(new SiteConfig().setSiteKey(key).setSiteVal(value));
        }
    }

    @Override
    @CacheEvict(value = "site", key = "'config'", allEntries = true)
    public void updateAll(Map<String, String> map, HttpServletRequest request, HttpServletResponse response) {
        map.forEach(this::updateByKey);
    }
}




