package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.mapper.CommentMapper;
import com.loncol.cms.model.po.Comment;
import com.loncol.cms.service.CommentService;
import com.loncol.cms.model.vo.CommentConditionVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 评论服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment>
    implements CommentService{

    private final CommentMapper commentMapper;

    @Override
    public IPage<Comment> selectComments(IPage<Comment> page, CommentConditionVo vo) {
        page.setRecords(commentMapper.selectComments(page, vo));
        return page;
    }

    @Override
    public int deleteBatch(Integer[] ids) {
        return commentMapper.deleteBatch(ids);
    }
}




