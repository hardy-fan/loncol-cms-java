package com.loncol.cms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.mapper.ArticleContentMapper;
import com.loncol.cms.mapper.ArticleMapper;
import com.loncol.cms.mapper.ArticleTagMapper;
import com.loncol.cms.mapper.TagMapper;
import com.loncol.cms.model.po.Article;
import com.loncol.cms.model.po.ArticleContent;
import com.loncol.cms.model.po.ArticleTag;
import com.loncol.cms.model.po.Tag;
import com.loncol.cms.service.ArticleContentService;
import com.loncol.cms.service.ArticleService;
import com.loncol.cms.service.ArticleTagService;
import com.loncol.cms.model.vo.ArticleConditionVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 文章服务
* @author Hardy
* @date 2022-05-08
*/
@Service
@AllArgsConstructor
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article>
    implements ArticleService{

    private final ArticleMapper articleMapper;
    private final TagMapper tagMapper;
    private final ArticleTagMapper articleTagMapper;
    private final ArticleTagService articleTagService;
    private final ArticleContentMapper articleContentMapper;
    private final ArticleContentService articleContentService;

    @Override
    public List<Article> findByCondition(IPage<Article> page, ArticleConditionVo vo) {
        List<Article> articleList = articleMapper.findByCondition(page, vo);

        if (CollUtil.isNotEmpty(articleList)) {
            // 文章Id列表
            List<Integer> ids = articleList.stream()
                    .map(Article::getId)
                    .collect(Collectors.toList());

            // 所有文章关联的Tag列表
            List<ArticleTag> articleTagList = articleTagMapper.selectList(new LambdaUpdateWrapper<ArticleTag>()
                    .in(ArticleTag::getArticleId, ids));

            //所有的tag Id列表
            List<Integer> tagIds = articleTagList.stream()
                    .map(ArticleTag::getTagId)
                    .collect(Collectors.toList());

            //所有的Tag列表
            List<Tag> tagList = tagMapper.selectList(new LambdaUpdateWrapper<Tag>()
                    .in(Tag::getId, tagIds));

            //设置每篇文章的Tag、热帖、新帖
            for (Article article : articleList) {
                // 热帖处理逻辑
                article.setHotFlag((article.getVisitCount() > 100)?1:0);
                //新帖处理逻辑
                if(article.getPublishDate() != null) {
                    article.setNewFlag((DateUtil.between(article.getPublishDate(),
                            new Date(), DateUnit.DAY)) < 5 ? 1 : 0);
                }
            }
        }
        return articleList;
    }

    @Override
    public List<Article> sliderList() {
        return articleMapper.selectList(null);
    }

    @Override
    public List<Article> recommendedList(int pageSize) {
        return null;
    }

    @Override
    public List<Article> recentList(int pageSize) {
        return null;
    }

    @Override
    public List<Article> randomList(int pageSize) {
        return null;
    }

    @Override
    public List<Article> hotList(int pageSize) {
        return null;
    }

    @Override
    public List<Article> selectByCategoryId(Integer categoryId) {
        return articleMapper.selectList(Wrappers.<Article>lambdaQuery()
                .eq(Article::getCategoryId, categoryId));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addAssignTags(Integer articleId, List<Integer> tagIds) {
        if(tagIds == null){
            articleTagMapper.delete(Wrappers.<ArticleTag>lambdaQuery()
                    .eq(ArticleTag::getArticleId, articleId));
            return;
        }

        articleTagMapper.delete(Wrappers.<ArticleTag>lambdaQuery()
                .eq(ArticleTag::getArticleId, articleId)
                .notIn(ArticleTag::getTagId, tagIds));
        List<Object> oldTagIds = articleTagMapper.selectObjs(Wrappers.<ArticleTag>lambdaQuery()
                        .select(ArticleTag::getTagId)
                        .eq(ArticleTag::getArticleId, articleId));
        if(tagIds.size() <= oldTagIds.size()){
            return;
        }
        tagIds.removeAll(Arrays.asList(Convert.toIntArray(oldTagIds)));
        articleTagService.insertList(tagIds, articleId);
    }

    @Override
    public void assignContent(Integer articleId, String content) {
        ArticleContent articleContent = new ArticleContent();
        articleContent.setArticleId(articleId);
        articleContent.setContent(content);
        articleContentService.saveOrUpdate(articleContent, new UpdateWrapper<ArticleContent>().lambda()
                .eq(ArticleContent::getArticleId, articleContent.getArticleId()));
    }

    @Override
    public Article getDetailById(Serializable id) {
        Article article = articleMapper.selectById(id);

        List<ArticleTag> articleTagList = articleTagMapper.selectList(new LambdaUpdateWrapper<ArticleTag>()
                .eq(ArticleTag::getArticleId, id));
        if(articleTagList.size()>0) {
            List<Integer> tagIds = articleTagList.stream()
                    .map(ArticleTag::getTagId)
                    .collect(Collectors.toList());
            List<Tag> tagList = tagMapper.selectList(new LambdaUpdateWrapper<Tag>()
                    .in(Tag::getId, tagIds));
            if (Objects.nonNull(tagList)) {
                article.setTags(tagList);
            }
        }
        article.setHotFlag((article.getVisitCount()> 100)?1:0);
        if(article.getPublishDate() != null) {
            article.setNewFlag((DateUtil.between(article.getPublishDate(), new Date(), DateUnit.DAY) < 5) ? 1 : 0);
        }
        String content = Optional.ofNullable(
                articleContentMapper.selectOne(new LambdaUpdateWrapper<ArticleContent>()
                .eq(ArticleContent::getArticleId, article.getId())))
                .map(ArticleContent::getContent).orElse("");
        article.setContent(content);

        return article;
    }
}




