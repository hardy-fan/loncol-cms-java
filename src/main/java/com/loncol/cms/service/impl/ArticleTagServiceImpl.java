package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.ArticleTag;
import com.loncol.cms.service.ArticleTagService;
import com.loncol.cms.mapper.ArticleTagMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文章标签服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class ArticleTagServiceImpl extends ServiceImpl<ArticleTagMapper, ArticleTag>
    implements ArticleTagService{

    private final ArticleTagMapper articleTagMapper;

    @Override
    public int removeByArticleId(Integer articleId) {
        return articleTagMapper.delete(Wrappers.<ArticleTag>lambdaQuery()
                .eq(ArticleTag::getArticleId, articleId));
    }

    @Override
    public void insertList(List<Integer> tagIds, Integer articleId) {
        for (Integer tagId : tagIds) {
            ArticleTag articleTag = new ArticleTag();
            articleTag.setTagId(tagId);
            articleTag.setArticleId(articleId);
            articleTagMapper.insert(articleTag);
        }
    }
}




