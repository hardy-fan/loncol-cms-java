package com.loncol.cms.service.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.loncol.cms.common.config.StaticConfig;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.vo.PaginationVo;
import com.loncol.cms.common.component.CommonDataService;
import com.loncol.cms.model.po.Article;
import com.loncol.cms.model.po.ArticleTag;
import com.loncol.cms.model.po.Category;
import com.loncol.cms.model.po.Tag;
import com.loncol.cms.service.*;
import com.loncol.cms.model.vo.ArticleConditionVo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 静态页面服务实现
 * @author Hardy
 * @date 2022-10-30
 */
@Slf4j
@Service
@AllArgsConstructor
public class StaticHtmlServiceImpl implements StaticHtmlService {

    private final StaticConfig staticConfig;
    private final TemplateEngine templateEngine;
    private final ArticleService articleService;
    private final CategoryService categoryService;
    private final TagService tagService;
    private final ArticleTagService articleTagService;
    private final SiteThemeService themeService;
    private final CommonDataService commonDataService;

    @Override
    public void makeStaticSite(HttpServletRequest request, HttpServletResponse response, Boolean force) {
        createIndexHtml(request, response, true);
        createArticleHtml(request, response, true);
        createCategoryHtml(request, response, true);
        createTagHtml(request, response, true);
        createCommmentHtml(request, response, true);
    }

    @Override
    public void createIndexHtml(HttpServletRequest request, HttpServletResponse response, Boolean force) {
        List<Article> sliderList = articleService.sliderList();
        ArticleConditionVo vo = new ArticleConditionVo();
        vo.setStatus(CoreConst.STATUS_VALID);
        int total = (int) articleService.count(
                Wrappers.<Article>lambdaQuery().eq(Article::getStatus, CoreConst.STATUS_VALID));
        if (total == 0) {
            Map<String, Object> paramMap = MapUtil.builder(new HashMap<String, Object>(5))
                    .put("pageUrl", "index")
                    .put("categoryId", "index")
                    .put("sliderList", sliderList)
                    .put("page", new PaginationVo<>(1, 10))
                    .put("articleList", Collections.emptyList())
                    .build();
            createHtml(request, response, force, paramMap, "index", "index");
            return;
        }
        IPage<Object> pagination = new PaginationVo<>(vo.getPageNum(), vo.getPageSize()).setTotal(total);
        for (long pageNum = 1; pageNum <= pagination.getPages(); pageNum++) {
            IPage<Article> page = new PaginationVo<>(vo.getPageNum(), vo.getPageSize());
            List<Article> articleList = articleService.findByCondition(page, vo);
            Map<String, Object> paramMap = MapUtil.builder(new HashMap<String, Object>(5))
                    .put("pageUrl", "index")
                    .put("categoryId", "index")
                    .put("sliderList", sliderList)
                    .put("page", page)
                    .put("articleList", articleList)
                    .build();
            if (pageNum == 1) {
                createHtml(request, response, force, paramMap,
                        "index", "index");
            }
            createHtml(request, response, force, paramMap,
                    "index", "index" + File.separator + pageNum);
        }
    }

    @Override
    public void createArticleHtml(HttpServletRequest request, HttpServletResponse response, Boolean force) {
        List<Article> articleList = articleService.findByCondition(
                new PaginationVo<>(1, 99999),
                new ArticleConditionVo().setStatus(CoreConst.STATUS_VALID));
        for (Article article : articleList) {
            Map<String, Object> paramMap = new HashMap<>(2);
            paramMap.put("article", article);
            paramMap.put("categoryId", article.getCategoryId());
            createHtml(request, response, force, paramMap,
                    "article", String.valueOf(article.getId()));
        }
    }

    @Override
    public void createCommmentHtml(HttpServletRequest request, HttpServletResponse response, Boolean force) {
        createHtml(request, response, force, Collections.emptyMap(),
                "comment", "comment");
    }

    @Override
    public void createCategoryHtml(HttpServletRequest request, HttpServletResponse response, Boolean force) {
        List<Category> categoryList = categoryService.list(
                Wrappers.<Category>lambdaQuery().eq(Category::getStatus, CoreConst.STATUS_VALID));
        for (Category category : categoryList) {
            ArticleConditionVo vo = new ArticleConditionVo();
            vo.setStatus(CoreConst.STATUS_VALID);
            vo.setCategoryId(category.getId());

            int total = (int) articleService.count(
                    Wrappers.<Article>lambdaQuery().
                            eq(Article::getCategoryId, category.getId())
                            .eq(Article::getStatus, CoreConst.STATUS_VALID));
            if (total == 0) {
                Map<String, Object> paramMap = MapUtil.builder(new HashMap<String, Object>(4))
                        .put("pageUrl", "category/" + category.getId())
                        .put("categoryId", category.getId())
                        .put("categoryName", category.getCategoryName())
                        .put("articleList", Collections.emptyList())
                        .build();

                createHtml(request, response, force, paramMap,
                        "index", "category" + File.separator + category.getId());
                continue;
            }
            IPage<Object> pagination = new PaginationVo<>(vo.getPageNum(), vo.getPageSize()).setTotal(total);
            for (long pageNum = 1; pageNum <= pagination.getPages(); pageNum++) {

                PaginationVo<Article> page = new PaginationVo<>(pageNum, vo.getPageSize());
                List<Article> articleList = articleService.findByCondition(page, vo);
                Map<String, Object> paramMap = MapUtil.builder(new HashMap<String, Object>(5))
                        .put("pageUrl", "category/" + category.getId())
                        .put("categoryId", category.getId())
                        .put("categoryName", category.getCategoryName())
                        .put("page", page)
                        .put("articleList", articleList)
                        .build();

                if (pageNum == 1) {
                    createHtml(request, response, force, paramMap,
                            "index", "category" + File.separator + category.getId());
                }
                createHtml(request, response, force, paramMap,
                        "index",
                        "category" + File.separator + category.getId() + File.separator + pageNum);
            }
        }
    }

    @Override
    public void createTagHtml(HttpServletRequest request, HttpServletResponse response, Boolean force) {
        List<Tag> tagsList = tagService.list();
        for (Tag tag : tagsList) {

            ArticleConditionVo vo = new ArticleConditionVo();
            //这里设置TagId，将按照tagId过滤文章
            vo.setTagId(tag.getId());

            // 标签下的文章数
            int total = (int) articleTagService.count(
                    Wrappers.<ArticleTag>lambdaQuery().eq(ArticleTag::
                            getTagId, tag.getId()));

            if (total == 0) {
                Map<String, Object> paramMap = MapUtil.builder(new HashMap<String, Object>(2))
                        .put("pageUrl", "tag/" + tag.getId())
                        .put("articleList", Collections.emptyList())
                        .build();

                createHtml(request, response, force, paramMap,
                        "index", "tag" + File.separator + tag.getId());
                continue;
            }

            IPage<Object> pagination = new PaginationVo<>(vo.getPageNum(), vo.getPageSize()).setTotal(total);
            for (long pageNum = 1; pageNum <= pagination.getPages(); pageNum++) {

                PaginationVo<Article> page = new PaginationVo<>(pageNum, vo.getPageSize());
                List<Article> articleList = articleService.findByCondition(page, vo);
                Map<String, Object> paramMap = MapUtil.builder(new HashMap<String, Object>(2))
                        .put("pageUrl", "tag/" + tag.getId())
                        .put("page", page)
                        .put("articleList", articleList)
                        .build();

                if (pageNum == 1) {
                    createHtml(request, response, force, paramMap,
                            "index", "tag" + File.separator + tag.getId());
                }
                createHtml(request, response, force, paramMap,
                        "index",
                        "tag" + File.separator + tag.getId() + File.separator + pageNum);
            }
        }
    }


    public void createHtml(HttpServletRequest request,
                           HttpServletResponse response,
                           Boolean force,
                           Map<String, Object> map,
                           String templateUrl,
                           String fileName) {
        if (StrUtil.isBlank(staticConfig.getFolder())) {
            throw new IllegalArgumentException("请先在Yml配置静态页面生成路径");
        }
        log.info("开始生成静态页面: {}", fileName);

        templateUrl = StrUtil.removePrefix(templateUrl, File.separator);

        PrintWriter writer = null;
        try {
            Map<String, Object> paramMap = commonDataService.getAllCommonData();
            // 获取页面数据
            paramMap.putAll(map);
            // 创建thymeleaf上下文对象
            WebContext context = new WebContext(request, response, request.getServletContext());
            // 把数据放入上下文对象
            context.setVariables(paramMap);
            // 文件生成路径
            String fileUrl = StrUtil.appendIfMissing(
                    staticConfig.getFolder(),
                    File.separator) + templateUrl + File.separator + fileName + ".html";
            // 自动创建上层文件夹



            File directory = new File(StrUtil.subBefore(fileUrl, File.separator, true));
            if (!directory.exists()) {
                directory.mkdirs();
            }
            // 创建输出流
            File file = new File(fileUrl);
            if (Boolean.FALSE.equals(force) && file.exists()) {
                // 不强制覆盖现有文件
                return;
            }
            writer = new PrintWriter(file);
            // 执行页面静态化方法
            templateEngine.process(CoreConst.THEME_PREFIX
                    + themeService.selectCurrent().getThemeName() + File.separator + templateUrl,
                    context, writer);
        } catch (Exception e) {
            log.error("页面静态化出错：{}", e.getMessage(), e);
        } finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
        }
    }


}
