package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.InfoBlock;
import com.loncol.cms.service.InfoBlockService;
import com.loncol.cms.mapper.InfoBlockMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 信息块服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class InfoBlockServiceImpl extends ServiceImpl<InfoBlockMapper, InfoBlock>
    implements InfoBlockService{

    private final InfoBlockMapper blockMapper;

    @Override
    public List<InfoBlock> findByCondition(InfoBlock vo) {
        return blockMapper.selectBlocks(vo);
    }

    @Override
    public List<InfoBlock> selectByPid(Integer pid) {
        return blockMapper.selectList(new LambdaQueryWrapper<InfoBlock>()
                .eq(InfoBlock::getParentId, pid));
    }

    @Override
    public List<InfoBlock> selectByPids(Integer[] pids) {
        return blockMapper.selectList(new LambdaQueryWrapper<InfoBlock>()
                .in(InfoBlock::getParentId, pids));
    }

    @Override
    public InfoBlock getDetailById(Integer id) {

        InfoBlock infoBlock = blockMapper.getDetailById(id);
        if(infoBlock.getParentId() != null){
            InfoBlock infoBlock1 = blockMapper.getDetailById(infoBlock.getParentId());
            infoBlock.setParent(infoBlock1);
        }
        return infoBlock;
    }
}




