package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.Category;
import com.loncol.cms.service.CategoryService;
import com.loncol.cms.mapper.CategoryMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 栏目服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category>
    implements CategoryService{

    private CategoryMapper categoryMapper;

    @Override
    public List<Category> selectCategories(Category categoryCondition) {
        return categoryMapper.selectCategories(categoryCondition);
    }

    @Override
    public List<Category> selectByPid(Integer pid) {
        return categoryMapper.selectList(new LambdaQueryWrapper<Category>()
                .eq(Category::getParentId, pid));
    }

    @Override
    public List<Category> selectByPids(Integer[] pids) {
        return categoryMapper.selectList(new LambdaQueryWrapper<Category>()
                .in(Category::getParentId, pids));
    }

    @Override
    public List<Category> selectByTypeId(Integer typeId) {
        return categoryMapper.selectList(new LambdaQueryWrapper<Category>()
                .eq(Category::getCategoryType, typeId)
                .orderByAsc(Category::getSort, Category::getId));
    }

    @Override
    public Category getDetailById(Integer id) {
        return categoryMapper.getDetailById(id);
    }

}




