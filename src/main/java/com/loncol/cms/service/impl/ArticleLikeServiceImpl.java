package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.ArticleLike;
import com.loncol.cms.service.ArticleLikeService;
import com.loncol.cms.mapper.ArticleLikeMapper;
import org.springframework.stereotype.Service;

/**
 * 文章点赞服务
 * @author hardy
 * @date 2022-10-30
 */
@Service
public class ArticleLikeServiceImpl extends ServiceImpl<ArticleLikeMapper, ArticleLike>
    implements ArticleLikeService{

}




