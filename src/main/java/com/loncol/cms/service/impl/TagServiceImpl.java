package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.vo.PaginationVo;
import com.loncol.cms.mapper.TagMapper;
import com.loncol.cms.model.po.Tag;
import com.loncol.cms.service.TagService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * 标签服务实现
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag>
    implements TagService{

    @Override
    public IPage<Tag> listByCondition(Tag tagQuery, Integer pageNum, Integer pageSize) {
        IPage<Tag> page = new PaginationVo<>(pageNum, pageSize);
        LambdaQueryWrapper<Tag> wrapper = new LambdaQueryWrapper<>();
        if(StringUtils.hasText(tagQuery.getTagName())){
            wrapper.like(Tag::getTagName, tagQuery.getTagName());
        }
        if(StringUtils.hasText(tagQuery.getTagDesc())){
            wrapper.like(Tag::getTagDesc, tagQuery.getTagDesc());
        }
        return this.page(page, wrapper);
    }
}




