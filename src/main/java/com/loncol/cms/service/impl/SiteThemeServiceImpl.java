package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.po.SiteTheme;
import com.loncol.cms.service.SiteThemeService;
import com.loncol.cms.mapper.SiteThemeMapper;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * 主题服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class SiteThemeServiceImpl extends ServiceImpl<SiteThemeMapper, SiteTheme>
    implements SiteThemeService{
    private final SiteThemeMapper themeMapper;

    @Override
    @CacheEvict(value = "theme", allEntries = true)
    public int useTheme(Integer id) {
        themeMapper.update(null, new LambdaUpdateWrapper<SiteTheme>()
                .set(SiteTheme::getStatus, 0).eq(SiteTheme::getStatus, 1));

        return themeMapper.update(null, new LambdaUpdateWrapper<SiteTheme>()
                .set(SiteTheme::getStatus,1).eq(SiteTheme::getId, id));
    }

    @Override
    @Cacheable(value = "SiteTheme", key = "'current'")
    public SiteTheme selectCurrent() {
        return themeMapper.selectOne(Wrappers.<SiteTheme>lambdaQuery()
                .eq(SiteTheme::getStatus, CoreConst.STATUS_VALID));
    }

    @Override
    @CacheEvict(value = "SiteTheme", allEntries = true)
    public int deleteBatch(Integer[] ids) {
        return themeMapper.deleteBatchIds(Arrays.asList(ids));
    }
}





