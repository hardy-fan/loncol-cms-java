package com.loncol.cms.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.vo.PaginationVo;
import com.loncol.cms.mapper.*;
import com.loncol.cms.model.po.*;
import com.loncol.cms.service.RolePermissionService;
import com.loncol.cms.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * 角色服务
 * @author Hardy
 * @date 2022-10-30
*/

@Service
@AllArgsConstructor
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role>
    implements RoleService{

    private final RoleMapper roleMapper;
    private final UserRoleMapper userRoleMapper;
    private final PermissionMapper permissionMapper;
    private final UserMapper userMapper;
    private final RolePermissionMapper rolePermissionMapper;
    private final RolePermissionService rolePermissionService;

    @Override
    public IPage<Role> listByCondition(Role roleQuery, Integer pageNum, Integer pageSize) {
        IPage<Role> page = new PaginationVo<>(pageNum, pageSize);
        LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
        if(org.springframework.util.StringUtils.hasText(roleQuery.getRoleName())){
            wrapper.like(Role::getRoleName, roleQuery.getRoleName());
        }
        if(org.springframework.util.StringUtils.hasText(roleQuery.getRoleName())){
            wrapper.like(Role::getRoleName, roleQuery.getRoleName());
        }
        return this.page(page, wrapper);
    }

    @Override
    public Set<Integer> findRoleIdsByUserId(Integer userId) {
        LambdaQueryWrapper<UserRole> queryWrapper = new QueryWrapper<UserRole>().lambda()
                .select(UserRole::getRoleId);
        //对于admin账号进行权限开放
        if(userId != 0){
            queryWrapper.eq(UserRole::getUserId, userId);
        }
        List<Object> roleIds = userRoleMapper.selectObjs(queryWrapper);
        return Convert.toSet(Integer.class, roleIds);
    }

    @Override
    public Set<Integer> findUserIdsByRoleId(Integer roleId) {
        List<Object> userIds = userRoleMapper.selectObjs( new QueryWrapper<UserRole>().lambda()
                .eq(UserRole::getRoleId, roleId)
                .select(UserRole::getUserId));
        return Convert.toSet(Integer.class, userIds);
    }

    @Override
    public Set<Integer> findUserIdsByRoleIds(List<Integer> roleIds) {
        List<Object> userIds = userRoleMapper.selectObjs( new QueryWrapper<UserRole>().lambda()
                .in(UserRole::getRoleId, roleIds)
                .select(UserRole::getUserId));
        return Convert.toSet(Integer.class, userIds);
    }

    @Override
    public List<Permission> findPermsByRoleId(Integer roleId) {
        return permissionMapper.selectList(new QueryWrapper<Permission>().lambda()
                .inSql(Permission::getId, "SELECT perm_id FROM auth_role_permission WHERE role_id="+roleId));
    }

    @Override
    public List<User> findUsersByRoleId(Integer roleId) {
        return userMapper.selectList(new QueryWrapper<User>().lambda()
                .inSql(User::getId, "SELECT user_id FROM auth_user_role WHERE role_id="+roleId));
    }

    @Override
    public int updateStatus(Integer roleId, Integer status) {
        return roleMapper.update(null, new LambdaUpdateWrapper<Role>()
                .set(Role::getStatus, status)
                .eq(Role::getId, roleId));
    }

    @Override
    public int updateStatusBatch(List<Integer> roleIds, Integer status) {
        return roleMapper.update(null, new LambdaUpdateWrapper<Role>()
                .set(Role::getStatus, status)
                .in(Role::getId, roleIds));
    }

    @Override
    public void addAssignPermission(Integer roleId, List<Integer> permIdList) {
        rolePermissionMapper.delete(Wrappers.<RolePermission>lambdaQuery()
                .eq(RolePermission::getRoleId, roleId)
                .notIn(RolePermission::getPermId, permIdList));

        for (Integer permId : permIdList) {
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(roleId).setPermId(permId);

            rolePermissionService.saveOrUpdate(rolePermission,
                    new UpdateWrapper<RolePermission>().lambda()
                            .eq(RolePermission::getRoleId, roleId)
                            .eq(RolePermission::getPermId, permId));
        }
    }
}




