package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.UserRole;
import com.loncol.cms.service.UserRoleService;
import com.loncol.cms.mapper.UserRoleMapper;
import org.springframework.stereotype.Service;

/**
 * 用户角色服务实现
 * @author Hardy
 * @date 2022-10-30
*/

@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole>
    implements UserRoleService{

}




