package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.RolePermission;
import com.loncol.cms.service.RolePermissionService;
import com.loncol.cms.mapper.RolePermissionMapper;
import org.springframework.stereotype.Service;

/**
 * 角色权限服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission>
    implements RolePermissionService{

}




