package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.DictConfig;
import com.loncol.cms.service.DictConfigService;
import com.loncol.cms.mapper.DictConfigMapper;
import org.springframework.stereotype.Service;

/**
 * 字典配置服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
public class DictConfigServiceImpl extends ServiceImpl<DictConfigMapper, DictConfig>
    implements DictConfigService{

}




