package com.loncol.cms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.po.Permission;
import com.loncol.cms.service.PermissionService;
import com.loncol.cms.mapper.PermissionMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 权限服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission>
    implements PermissionService{

    private static final Pattern SLASH_PATTERN = Pattern.compile("/");

    private final PermissionMapper permissionMapper;

    @Override
    public Set<String> findMarksByUserId(Integer userId) {
        return permissionMapper.findMarksByUserId(userId);
    }

    @Override
    public List<Permission> selectAll(Integer status) {
        return permissionMapper.selectList(Wrappers.<Permission>lambdaQuery()
                .eq(Permission::getStatus, CoreConst.STATUS_VALID)
                .orderByAsc(Permission::getSort));
    }

    @Override
    public List<Permission> selectAllMenuName(Integer status) {
        return permissionMapper.selectList(Wrappers.<Permission>lambdaQuery()
                .eq(Permission::getStatus, CoreConst.STATUS_VALID)
                .in(Permission::getPermType, Arrays.asList("1","2"))
                .orderByAsc(Permission::getSort));
    }

    @Override
    public Long getSubCountByPermId(Integer permId) {
        return permissionMapper.selectCount(Wrappers.<Permission>lambdaQuery()
                .eq(Permission::getParentId, permId).eq(Permission::getStatus, CoreConst.STATUS_VALID));
    }

    @Override
    public Boolean updateStatus(Integer id, Integer status) {
        return update(Wrappers.<Permission>lambdaUpdate()
                .eq(Permission::getId, id)
                .set(Permission::getStatus, status));
    }

    @Override
    public List<Permission> selectMenuByUserId(Integer userId) {
        return permissionMapper.selectMenuByUserId(userId);
    }

    @Override
    public List<Permission> selectMenuTreeByUserId(Integer userId) {
        return buildPermissionTree(permissionMapper.selectMenuByUserId(userId));
    }

    private static List<Permission> buildPermissionTree(List<Permission> permissionList) {
        Map<Integer, List<Permission>> parentIdToPermissionListMap = permissionList.stream().peek(p -> {
            if (StrUtil.startWith(p.getUrl(), "/")) {
                p.setUrl(SLASH_PATTERN.matcher(p.getUrl()).replaceFirst("#"));
            }
        }).collect(Collectors.groupingBy(Permission::getParentId));
        List<Permission> rootLevelPermissionList = parentIdToPermissionListMap.getOrDefault(CoreConst.TOP_MENU_ID, Collections.emptyList());
        fetchChildren(rootLevelPermissionList, parentIdToPermissionListMap);
        return rootLevelPermissionList;
    }

    private static void fetchChildren(List<Permission> permissionList, Map<Integer, List<Permission>> parentIdToPermissionListMap) {
        if (CollUtil.isEmpty(permissionList)) {
            return;
        }
        for (Permission permission : permissionList) {
            List<Permission> childrenList = parentIdToPermissionListMap.get(permission.getId());
            fetchChildren(childrenList, parentIdToPermissionListMap);
            permission.setChildren(childrenList);
        }
    }

    @Override
    public boolean save(Permission entity) {
        entity.setStatus(CoreConst.STATUS_VALID);
        return super.save(entity);
    }
}




