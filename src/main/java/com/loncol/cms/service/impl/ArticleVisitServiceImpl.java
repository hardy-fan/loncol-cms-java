package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.ArticleVisit;
import com.loncol.cms.service.ArticleVisitService;
import com.loncol.cms.mapper.ArticleVisitMapper;
import org.springframework.stereotype.Service;

/**
 * 文章访问服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
public class ArticleVisitServiceImpl
        extends ServiceImpl<ArticleVisitMapper, ArticleVisit>
        implements ArticleVisitService{
}




