package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.mapper.CategoryMapper;
import com.loncol.cms.model.po.Category;
import com.loncol.cms.model.po.CategoryContent;
import com.loncol.cms.service.CategoryContentService;
import com.loncol.cms.mapper.CategoryContentMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 栏目内容服务
 * @author hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class CategoryContentServiceImpl extends ServiceImpl<CategoryContentMapper, CategoryContent>
    implements CategoryContentService{

    private CategoryMapper categoryMapper;

    private CategoryContentMapper categoryContentMapper;

    @Override
    public CategoryContent getByCategoryId(Integer categoryId) {

        CategoryContent categoryContent = categoryContentMapper.selectOne(Wrappers.<CategoryContent>lambdaQuery()
                .eq(CategoryContent::getCategoryId, categoryId));

        if(categoryContent == null){
            Category category = categoryMapper.selectById(categoryId);
            categoryContent = new CategoryContent();
            categoryContent.setTitle(category.getCategoryName());
            categoryContent.setCategoryId(categoryId);
            categoryContent.setStatus(1);
        }

        return categoryContent;
    }
}




