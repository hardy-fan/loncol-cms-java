package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.vo.PaginationVo;
import com.loncol.cms.model.po.InfoPosition;
import com.loncol.cms.service.InfoPositionService;
import com.loncol.cms.mapper.InfoPositionMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * 信息位服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
public class InfoPositionServiceImpl extends ServiceImpl<InfoPositionMapper, InfoPosition>
    implements InfoPositionService{

    @Override
    public IPage<InfoPosition> listByCondition(InfoPosition query, Integer pageNum, Integer pageSize) {
        IPage<InfoPosition> page = new PaginationVo<>(pageNum, pageSize);
        LambdaQueryWrapper<InfoPosition> wrapper = new LambdaQueryWrapper<>();
        if(StringUtils.hasText(query.getPositionName())){
            wrapper.like(InfoPosition::getPositionName, query.getPositionName());
        }
        if(StringUtils.hasText(query.getPositionDesc())){
            wrapper.like(InfoPosition::getPositionDesc, query.getPositionDesc());
        }
        return this.page(page, wrapper);
    }
}




