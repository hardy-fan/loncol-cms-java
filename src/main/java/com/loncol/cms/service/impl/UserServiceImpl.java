package com.loncol.cms.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.vo.PaginationVo;
import com.loncol.cms.mapper.UserRoleMapper;
import com.loncol.cms.model.po.User;
import com.loncol.cms.model.po.UserRole;
import com.loncol.cms.service.UserRoleService;
import com.loncol.cms.service.UserService;
import com.loncol.cms.mapper.UserMapper;
import com.loncol.cms.model.vo.UserOnlineVo;
import lombok.AllArgsConstructor;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.apache.shiro.util.ByteSource;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.*;

/**
 * 用户服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{

    private final UserMapper userMapper;
    private final UserRoleMapper userRoleMapper;
    private final UserRoleService userRoleService;

    private final SessionManager sessionManager;
    private final RedisCacheManager redisCacheManager;
    private final RedisSessionDAO redisSessionDAO;

    private static final RandomNumberGenerator RANDOM_NUMBER_GENERATOR = new SecureRandomNumberGenerator();
    private static final String ALGORITHM_NAME = "md5";
    private static final int HASH_ITERATIONS = 2;

    @Override
    public IPage<User> listByCondition(User userQuery, Integer pageNum, Integer pageSize) {
        IPage<User> page = new PaginationVo<>(pageNum, pageSize);
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        if(org.springframework.util.StringUtils.hasText(userQuery.getUsername())){
            wrapper.like(User::getUsername, userQuery.getUsername());
        }
        if(org.springframework.util.StringUtils.hasText(userQuery.getEmail())){
            wrapper.like(User::getEmail, userQuery.getEmail());
        }
        if(org.springframework.util.StringUtils.hasText(userQuery.getPhone())){
            wrapper.like(User::getPhone, userQuery.getPhone());
        }
        return this.page(page, wrapper);
    }

    @Override
    public User selectByUsername(String username) {
        return userMapper.selectOne(Wrappers.<User>lambdaQuery()
                .eq(User::getUsername, username)
                .eq(User::getStatus, CoreConst.STATUS_VALID));
    }

    @Override
    public boolean updateStatus(Integer id, Integer status) {
        return update(Wrappers.<User>lambdaUpdate()
                .eq(User::getId, id)
                .set(User::getStatus, status)
                .set(User::getUpdateTime, new Date()));
    }

    @Override
    public boolean updateStatusBatch(List<Integer> ids, Integer status) {
        return update(Wrappers.<User>lambdaUpdate()
                .in(User::getId, ids)
                .set(User::getStatus, status)
                .set(User::getUpdateTime, new Date()));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addAssignRole(Integer userId, List<Integer> roleIds) {

        //删除需要删除的角色
        userRoleMapper.delete(Wrappers.<UserRole>lambdaQuery()
                .eq(UserRole::getUserId, userId)
                .notIn(UserRole::getRoleId, roleIds));

        //查询用户现有的角色
        List<Object> oldRoleIds = userRoleMapper.selectObjs(Wrappers.<UserRole>lambdaQuery()
                .select(UserRole::getRoleId)
                .eq(UserRole::getUserId, userId));
        roleIds.removeAll(Arrays.asList(Convert.toIntArray(oldRoleIds)));
        //添加时，去除用户现有的角色

        List<UserRole> userRoleList = null;
        for (Integer roleId : roleIds) {
            UserRole userRole = new UserRole().setUserId(userId).setRoleId(roleId);
            if(userRole != null) {
                userRoleList.add(userRole);
            }
        }
        userRoleService.saveBatch(userRoleList);
    }

    @Override
    public void updateLastLogin(User user) {
        Assert.notNull(user, "param: user is null");
        user.setLastLogin(new Date());
        userMapper.updateById(user);
    }

    @Override
    public List<UserOnlineVo> selectOnlineUsers(UserOnlineVo userVo) {
        // 因为我们是用redis实现了shiro的session的Dao,而且是采用了shiro+redis这个插件
        // 所以从spring容器中获取redisSessionDAO
        // 来获取session列表.
        Collection<Session> sessions = redisSessionDAO.getActiveSessions();
        Iterator<Session> it = sessions.iterator();
        List<UserOnlineVo> onlineUserList = new ArrayList<>();
        // 遍历session
        while (it.hasNext()) {
            // 这是shiro已经存入session的
            // 现在直接取就是了
            Session session = it.next();
            //标记为已提出的不加入在线列表
            if (session.getAttribute("kickout") != null) {
                continue;
            }
            UserOnlineVo onlineUser = getSessionBo(session);
            if (onlineUser != null) {
                /*用户名搜索*/
                if (StrUtil.isNotBlank(userVo.getUsername())) {
                    if (onlineUser.getUsername().contains(userVo.getUsername())) {
                        onlineUserList.add(onlineUser);
                    }
                } else {
                    onlineUserList.add(onlineUser);
                }
            }
        }
        return onlineUserList;
    }

    @Override
    public void kickout(Serializable sessionId, String username) {
        getSessionBySessionId(sessionId).setAttribute("kickout", true);
        Cache<String, Deque<Serializable>> cache = redisCacheManager.getCache(CoreConst.SHIRO_REDIS_CACHE_NAME);
        Deque<Serializable> deques = cache.get(username);
        for (Serializable deque : deques) {
            if (sessionId.equals(deque)) {
                deques.remove(deque);
                break;
            }
        }
        cache.put(username, deques);
    }

    @Override
    public void encryptPassword(User user) {
        user.setSalt(RANDOM_NUMBER_GENERATOR.nextBytes().toHex());
        String newPassword = new SimpleHash(ALGORITHM_NAME,
                user.getPassword(),
                ByteSource.Util.bytes(user.getCredentialsSalt()),
                HASH_ITERATIONS).toHex();
        user.setPassword(newPassword);
    }

    @Override
    public String getEncryptPassword(User user) {
        return new SimpleHash(ALGORITHM_NAME,
                user.getPassword(),
                ByteSource.Util.bytes(user.getCredentialsSalt()),
                HASH_ITERATIONS).toHex();
    }

    private Session getSessionBySessionId(Serializable sessionId) {
        return sessionManager.getSession(new DefaultSessionKey(sessionId));
    }

    private static UserOnlineVo getSessionBo(Session session) {
        //获取session登录信息。
        Object obj = session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
        if (null == obj) {
            return null;
        }
        if (obj instanceof SimplePrincipalCollection) {
            SimplePrincipalCollection spc = (SimplePrincipalCollection) obj;
            obj = spc.getPrimaryPrincipal();
            if (obj instanceof User) {
                User user = (User) obj;
                UserOnlineVo userBo = new UserOnlineVo();
                userBo.setLastAccess(session.getLastAccessTime());
                userBo.setHost(user.getLoginIp());
                userBo.setSessionId(session.getId().toString());
                userBo.setLastLoginTime(user.getLastLogin());
                userBo.setTimeout(session.getTimeout());
                userBo.setStartTime(session.getStartTimestamp());
                userBo.setSessionStatus(false);
                userBo.setUsername(user.getUsername());
                return userBo;
            }
        }
        return null;
    }

}




