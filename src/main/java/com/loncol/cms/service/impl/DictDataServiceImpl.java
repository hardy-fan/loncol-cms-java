package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.DictData;
import com.loncol.cms.service.DictDataService;
import com.loncol.cms.mapper.DictDataMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典数据服务
 * @author Hardy
 * @date 2022-10-30
*/
@Service
@AllArgsConstructor
public class DictDataServiceImpl extends ServiceImpl<DictDataMapper, DictData>
    implements DictDataService{

    private DictDataMapper dictDataMapper;

    @Override
    public List<DictData> selectDictDataList(Integer typeId) {
        return dictDataMapper.selectList(Wrappers.<DictData>lambdaQuery()
                .eq(DictData::getTypeId, typeId)
                .eq(DictData::getStatus, 1)
                .orderByAsc(DictData::getCode));
    }
}




