package com.loncol.cms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.loncol.cms.common.config.FileConfig;
import com.loncol.cms.common.exception.UploadFileNotFoundException;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.FileUploadUtil;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.service.OssService;
import com.loncol.cms.model.vo.ResponseVo;
import com.loncol.cms.model.vo.UploadResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * OSS服务类
 * @author Hardy
 * @date 2020/3/31
 */
@Slf4j
@Service
public class OssServiceImpl implements OssService {
    @Resource
    private FileConfig fileConfig;

    @Override
    @SneakyThrows
    public UploadResponse upload(MultipartFile file) {
        if (file == null || file.isEmpty()) {
            throw new UploadFileNotFoundException(UploadResponse.ErrorEnum.FILE_NOT_FOUND.msg);
        }
        String originalFilename = file.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf('.')).toLowerCase();
        String relativePath = FileUploadUtil.uploadLocal(file, fileConfig.getUploadFolder());
        String accessPrefixUrl = fileConfig.getAccessPrefixUrl();
        if (!StrUtil.endWith(accessPrefixUrl, "/")) {
            accessPrefixUrl += '/';
        }
        String url = accessPrefixUrl + relativePath;
        ResponseVo<?> responseVo = ResultUtil.success();

        if (responseVo.getStatus().equals(CoreConst.SUCCESS_CODE)) {
            return UploadResponse.success(url, originalFilename, suffix, url, CoreConst.SUCCESS_CODE);
        } else {
            return UploadResponse.failed(originalFilename, CoreConst.FAIL_CODE, responseVo.getMsg());
        }
    }
}
