package com.loncol.cms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.loncol.cms.model.po.ArticleContent;
import com.loncol.cms.service.ArticleContentService;
import com.loncol.cms.mapper.ArticleContentMapper;
import org.springframework.stereotype.Service;

/**
 * 文件内容服务实现
 * @author Hardy
 * @date 2022-10-30
*/
@Service
public class ArticleContentServiceImpl extends ServiceImpl<ArticleContentMapper, ArticleContent>
    implements ArticleContentService{
}




