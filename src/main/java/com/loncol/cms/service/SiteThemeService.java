package com.loncol.cms.service;

import com.loncol.cms.model.po.SiteTheme;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统主题服务
* @author Hardy
* @date 2022-10-30
*/
public interface SiteThemeService extends IService<SiteTheme> {

    /**
     * 使用指定的主题
     * @param id
     * @return
     */
    int useTheme(Integer id);

    /**
     * 获得当前主题
     * @return
     */
    SiteTheme selectCurrent();

    /**
     * 批量删除主题
     * @param ids
     * @return
     */
    int deleteBatch(Integer[] ids);
}
