package com.loncol.cms.service;

import com.loncol.cms.model.po.ArticleVisit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 文章访问服务
* @author Hardy
* @date 2022-05-08
*/
public interface ArticleVisitService extends IService<ArticleVisit> {
}
