package com.loncol.cms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.model.po.InfoPosition;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 信息位服务
* @author Hardy
* @date 2022-05-08
*/
public interface InfoPositionService extends IService<InfoPosition> {
    /**
     * 根据查询条件获得分页列表数据
     * @param query 查询条件对象
     * @param pageNum 页码
     * @param pageSize 分页设置
     * @return
     */
    IPage<InfoPosition> listByCondition(InfoPosition query, Integer pageNum, Integer pageSize);
}
