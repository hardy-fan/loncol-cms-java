package com.loncol.cms.service;

import com.loncol.cms.model.vo.UploadResponse;
import org.springframework.web.multipart.MultipartFile;

/**
 * oss服务
 * @author Hardy
 * @date 2022-10-30
 */
public interface OssService {
    /**
     * 上传文件
     * @param file 文件
     * @return
     */
    UploadResponse upload(MultipartFile file);
}
