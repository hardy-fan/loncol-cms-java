package com.loncol.cms.service;

import com.loncol.cms.model.po.SiteConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 站点配置服务
* @author hardy
* @date 2022-10-30
*/
public interface SiteConfigService extends IService<SiteConfig> {
    /**
     * 获得系统配置参数
     * @return
     */
    Map<String, String> selectAll();

    /**
     * 更新一个键值对
     * @param key
     * @param value
     * @return
     */
    boolean updateByKey(String key, String value);

    /**
     * 全部更新
     * @param map 映射
     * @param request 请求
     * @param response 响应
     */
    void updateAll(Map<String, String> map, HttpServletRequest request, HttpServletResponse response);
}
