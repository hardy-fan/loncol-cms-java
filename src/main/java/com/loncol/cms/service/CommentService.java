package com.loncol.cms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.loncol.cms.model.po.Comment;
import com.loncol.cms.model.vo.CommentConditionVo;

/**
 * 评论服务
 * @author Hardy
 * @date 2022-05-08
*/
public interface CommentService extends IService<Comment> {

    /**
     * 获取评论分页列表
     * @param page
     * @param vo
     * @return
     */
    IPage<Comment> selectComments(IPage<Comment> page, CommentConditionVo vo);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deleteBatch(Integer[] ids);
}
