package com.loncol.cms.service;

import com.loncol.cms.model.po.InfoBlock;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 信息服务
* @author Hardy
* @date 2022-05-08
*/
public interface InfoBlockService extends IService<InfoBlock> {
    /**
     * 查询信息块列表
     * @param vo
     * @return
     */
    List<InfoBlock> findByCondition(InfoBlock vo);

    /**
     * 根据父Id查询
     * @param pid
     * @return
     */
    List<InfoBlock> selectByPid(Integer pid);

    /**
     * 根据父Id列表查询
     * @param pids
     * @return
     */
    List<InfoBlock> selectByPids(Integer[] pids);

    /**
     * 根据Id查询一条
     * @param id
     * @return
     */
    InfoBlock getDetailById(Integer id);
}
