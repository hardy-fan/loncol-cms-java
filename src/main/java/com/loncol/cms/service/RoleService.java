package com.loncol.cms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.model.po.Permission;
import com.loncol.cms.model.po.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.loncol.cms.model.po.User;

import java.util.List;
import java.util.Set;

/**
 * 角色服务
* @author Hardy
* @date 2022-10-30
*/
public interface RoleService extends IService<Role> {

    /**
     * 根据条件分页查询
     * @param roleQuery
     * @param pageNum
     * @param pageSize
     * @return
     */
    IPage<Role> listByCondition(Role roleQuery, Integer pageNum, Integer pageSize);

    /**
     * 根据用户id查询角色集合
     *
     * @param userId
     * @return set
     */
    Set<Integer> findRoleIdsByUserId(Integer userId);

    /**
     * 根据角色编号获得用户列表
     * @param roleId
     * @return
     */
    Set<Integer> findUserIdsByRoleId(Integer roleId);

    /**
     * 获得用户编号列表
     * @param roleIds 角色编号列表
     * @return
     */
    Set<Integer> findUserIdsByRoleIds(List<Integer> roleIds);

    /**
     * 获得权限列表
     * @param roleId 角色编号
     * @return
     */
    List<Permission> findPermsByRoleId(Integer roleId);

    /**
     * 获得用户列表
     * @param roleId 角色编号
     * @return
     */
    List<User> findUsersByRoleId(Integer roleId);

    /**
     * 更新状态
     * @param roleId 角色编号
     * @param status 状态
     * @return
     */
    int updateStatus(Integer roleId, Integer status);

    /**
     * 批量更新状态
     *
     * @param roleIds
     * @param status
     * @return int
     */
    int updateStatusBatch(List<Integer> roleIds, Integer status);

    /**
     * 根据角色id保存分配权限
     *
     * @param roleId
     * @param permissionIdsList
     */
    void addAssignPermission(Integer roleId, List<Integer> permissionIdsList);
}
