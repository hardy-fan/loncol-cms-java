package com.loncol.cms.service;

import com.loncol.cms.model.po.ArticleTag;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 文章标签服务
* @author Hardy
* @date 2022-05-09
*/
public interface ArticleTagService extends IService<ArticleTag> {
    /**
     * 通过文章id删除文章-标签关联数据
     * @param articleId
     * @return
     */
    int removeByArticleId(Integer articleId);

    /**
     * 批量添加
     *  @param tagIds
     * @param articleId
     */
    void insertList(List<Integer> tagIds, Integer articleId);
}
