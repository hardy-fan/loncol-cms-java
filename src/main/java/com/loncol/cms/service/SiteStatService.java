package com.loncol.cms.service;

import com.loncol.cms.model.po.SiteStat;
import com.baomidou.mybatisplus.extension.service.IService;
import com.loncol.cms.model.vo.StatisticVo;

import java.util.Map;

/**
 * 网站统计服务
* @author Hardy
* @date 2022-10-30
*/
public interface SiteStatService extends IService<SiteStat> {

    /**
     * 获得站点信息
     * @return
     */
    Map<String, Object> getSiteInfo();

    /**
     * 获得静态配置信息
     * @return
     */
    StatisticVo indexStatistic();
}
