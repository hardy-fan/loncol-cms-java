package com.loncol.cms.service;

import com.loncol.cms.model.po.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户角色服务
* @author Hardy
* @date 2022-10-30
*/
public interface UserRoleService extends IService<UserRole> {
}
