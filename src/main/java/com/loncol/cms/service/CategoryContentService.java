package com.loncol.cms.service;

import com.loncol.cms.model.po.CategoryContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 单页服务
* @author hardy
* @date 2022-06-10
*/
public interface CategoryContentService extends IService<CategoryContent> {
    /**
     * 根据栏目id查询
     * @param categoryId
     * @return
     */
    CategoryContent getByCategoryId(Integer categoryId);
}
