package com.loncol.cms.service;

import com.loncol.cms.model.po.ArticleContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 文章内容服务
* @author Hardy
* @date 2022-05-08
*/
public interface ArticleContentService extends IService<ArticleContent> {
}
