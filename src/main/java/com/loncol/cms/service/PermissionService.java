package com.loncol.cms.service;

import com.loncol.cms.model.po.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * 权限服务
* @author Hardy
* @date 2022-10-30
*/
public interface PermissionService extends IService<Permission> {
    /**
     * 获得权限标识
     * @param userId
     * @return
     */
    Set<String> findMarksByUserId(Integer userId);

    /**
     * 获得全部权限
     * @param status
     * @return
     */
    List<Permission> selectAll(Integer status);

    /**
     * 获得全部菜单名称
     * @param status 状态
     * @return
     */
    List<Permission> selectAllMenuName(Integer status);

    /**
     * 获得子权限数量
     * @param permId 编号
     * @return
     */
    Long getSubCountByPermId(Integer permId);

    /**
     * 更新状态
     * @param id
     * @param status
     * @return
     */
    Boolean updateStatus(Integer id, Integer status);

    /**
     * 获得菜单列表
     * @param userId 用户编号
     * @return
     */
    List<Permission> selectMenuByUserId(Integer userId);

    /**
     * 获得树形菜单列表
     * @param userId 用户编号
     * @return
     */
    List<Permission> selectMenuTreeByUserId(Integer userId);
}
