package com.loncol.cms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.loncol.cms.model.po.Tag;

/**
 * 标签服务
* @author Hardy
* @date 2022-10-30
*/
public interface TagService extends IService<Tag> {

    /**
     * 根据查询条件获得分页列表数据
     * @param tagQuery 查询条件对象
     * @param pageNum 页码
     * @param pageSize 分页设置
     * @return
     */
    IPage<Tag> listByCondition(Tag tagQuery, Integer pageNum, Integer pageSize);
}
