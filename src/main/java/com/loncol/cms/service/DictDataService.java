package com.loncol.cms.service;

import com.loncol.cms.model.po.DictData;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 字典数据服务
* @author Hardy
* @date 2022-05-08
*/
public interface DictDataService extends IService<DictData> {
    /**
     * 获得字典列表
     * @param typeId 字典类型
     * @return
     */
    List<DictData> selectDictDataList(Integer typeId);
}
