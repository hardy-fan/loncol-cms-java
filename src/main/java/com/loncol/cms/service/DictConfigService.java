package com.loncol.cms.service;

import com.loncol.cms.model.po.DictConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 字典配置服务
* @author Hardy
* @date 2022-05-08
*/
public interface DictConfigService extends IService<DictConfig> {
}
