package com.loncol.cms.service;

import com.loncol.cms.model.po.DictType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 字典类型服务
* @author Hardy
* @date 2022-05-08
*/
public interface DictTypeService extends IService<DictType> {
}
