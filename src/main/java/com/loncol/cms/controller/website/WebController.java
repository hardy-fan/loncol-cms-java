package com.loncol.cms.controller.website;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.common.exception.ArticleNotFoundException;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.vo.PaginationVo;
import com.loncol.cms.model.po.Article;
import com.loncol.cms.model.po.Category;
import com.loncol.cms.service.ArticleService;
import com.loncol.cms.service.CategoryService;
import com.loncol.cms.service.SiteThemeService;
import com.loncol.cms.model.vo.ArticleConditionVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

import static com.loncol.cms.common.util.CoreConst.THEME_PREFIX;

/**
 * Web页面相关接口
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class WebController {

    private final ArticleService articleService;
    private final CategoryService categoryService;
    private final SiteThemeService themeService;

    /**
     * 首页
     * @param model 模型
     * @param vo 查询对象
     * @return
     */
    @GetMapping({"/", "/index/{pageNum}"})
    public String index(@PathVariable(value = "pageNum", required = false) Integer pageNum,
                        ArticleConditionVo vo,
                        Model model) {
        if (CoreConst.SITE_STATIC.get()) {
            return "forward:/html/index/index.html";
        }
        if (pageNum != null) {
            vo.setPageNum(pageNum);
        } else {
            // TODO：轮播文章
            model.addAttribute("sliderList", articleService.sliderList());
        }
        model.addAttribute("pageUrl", "index");
        model.addAttribute("categoryId", "index");
        loadMainPage(model, vo);
        return THEME_PREFIX + themeService.selectCurrent().getThemeName() + "/index";
    }

    /**
     * 分类列表
     *
     * @param categoryId 栏目编号
     * @param pageNum   页码
     * @param model 模型
     * @return
     */
    @GetMapping({"/category/{categoryId}", "/category/{categoryId}/{pageNum}"})
    public String category(@PathVariable("categoryId") Integer categoryId,
                           @PathVariable(value = "pageNum", required = false) Integer pageNum,
                           Model model) {
        if (CoreConst.SITE_STATIC.get()) {
            return String.join("",
                    "forward:/html/index/category/",
                    pageNum == null ? categoryId.toString() : categoryId + "/" + pageNum,
                    ".html");
        }
        ArticleConditionVo vo = new ArticleConditionVo();
        vo.setCategoryId(categoryId);
        if (pageNum != null) {
            vo.setPageNum(pageNum);
        }
        model.addAttribute("pageUrl", "category/" + categoryId);
        model.addAttribute("categoryId", categoryId);
        loadMainPage(model, vo);
        return THEME_PREFIX + themeService.selectCurrent().getThemeName() + "/index";
    }


    /**
     * 标签列表
     * TODO: 这里需要修改
     *
     * @param tagId 标签编号
     * @param model 模型
     * @return
     */
    @GetMapping({"/tag/{tagId}", "/tag/{tagId}/{pageNum}"})
    public String tag(@PathVariable("tagId") Integer tagId,
                      @PathVariable(value = "pageNum", required = false) Integer pageNum,
                      Model model) {
        if (CoreConst.SITE_STATIC.get()) {
            return "forward:/html/index/tag/"+ (pageNum == null ? tagId : tagId + "/" + pageNum)  +".html";
        }
        return THEME_PREFIX + themeService.selectCurrent().getThemeName() + "/index";
    }

    /**
     * 文章详情
     * @param model 模型
     * @param id  文章Id
     * @return
     */
    @GetMapping("/article/{id}")
    public String article(HttpServletRequest request, Model model, @PathVariable("id") Integer id) {
        if (CoreConst.SITE_STATIC.get()) {
            return "forward:/html/article/" + id + ".html";
        }
        Article article = articleService.getById(id);
        if (article == null || CoreConst.STATUS_INVALID.equals(article.getStatus())) {
            throw new ArticleNotFoundException();
        }
        model.addAttribute("article", article);
        model.addAttribute("categoryId", article.getCategoryId());
        return THEME_PREFIX + themeService.selectCurrent().getThemeName() + "/article";
    }

    /**
     * 评论
     * @param model 模型
     * @return
     */
    @GetMapping("/comment")
    public String comment(Model model) {
        if (CoreConst.SITE_STATIC.get()) {
            return "forward:/html/comment/comment.html";
        }
        model.addAttribute("categoryId", "comment");
        return THEME_PREFIX + themeService.selectCurrent().getThemeName() + "/comment";
    }

    /**
     * 装载主页面
     * @param model
     * @param vo
     */
    private void loadMainPage(Model model, ArticleConditionVo vo) {
        vo.setStatus(CoreConst.STATUS_VALID);
        IPage<Article> page = new PaginationVo<>(vo.getPageNum(), vo.getPageSize());
        List<Article> articleList = articleService.findByCondition(page, vo);
        model.addAttribute("page", page);
        model.addAttribute("articleList", articleList);
        if (vo.getCategoryId() != null) {
            Category category = categoryService.getById(vo.getCategoryId());
            if (category != null) {
                model.addAttribute("categoryName", category.getCategoryName());
            }
        }
    }

}
