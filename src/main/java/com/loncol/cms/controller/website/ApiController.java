package com.loncol.cms.controller.website;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.net.NetUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HtmlUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.loncol.cms.common.util.*;
import com.loncol.cms.model.po.ArticleLike;
import com.loncol.cms.model.po.ArticleVisit;
import com.loncol.cms.model.po.Comment;
import com.loncol.cms.service.ArticleLikeService;
import com.loncol.cms.service.ArticleVisitService;
import com.loncol.cms.service.CommentService;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * 为前端提供的接口
 * 包括针对文章进行的添加评论、获取评论、增加浏览次数和点赞操作
 * @author hardy
 * @date 2022/05/02
 */
@Slf4j
@RestController
@RequestMapping("api")
@AllArgsConstructor
public class ApiController {

    private final CommentService commentService;
    private final ArticleVisitService articleVisitService;
    private final ArticleLikeService articleLikeService;

    /**
     * 查询评论
     * @param condition 条件
     * @param pageNum 页码
     * @param pageSize 每页条数
     * @return
     */
    @PostMapping("comments")
    public IPage<Comment> getComments(String condition, Integer pageNum, Integer pageSize) {
        return null;
    }

    /**
     * 保存评论
     * @param request 请求
     * @param comment 评论
     * @return
     * @throws UnsupportedEncodingException
     */
    @PostMapping("comment/save")
    public ResponseVo saveComment(HttpServletRequest request, Comment comment)
            throws UnsupportedEncodingException {
        if (StringUtils.isEmpty(comment.getNickname())) {
            return ResultUtil.error("请输入昵称");
        }
        if (StringUtils.isNotBlank(comment.getEmail()) && !Validator.isEmail(comment.getEmail())) {
            return ResultUtil.error("邮箱格式不正确");
        }
        Date date = new Date();
        comment.setNickname(HtmlUtil.filter(comment.getNickname()));
        comment.setContent(HtmlUtil.filter(comment.getContent()));
        comment.setUserIp(NetUtil.ipv4ToLong(IpUtil.getIpStr(request)));
        comment.setCreateTime(date);
        comment.setUpdateTime(date);
        if (StringUtils.isNotBlank(comment.getQq())) {
            comment.setAvatar("http://q1.qlogo.cn/g?b=qq&nk=" + comment.getQq() + "&s=100");
        } else if (StringUtils.isNotBlank(comment.getEmail())) {
            String entry = DigestUtil.md5Hex(comment.getEmail());
            comment.setAvatar("http://www.gravatar.com/avatar/" + entry + "?d=mp");
        }
        boolean a = commentService.save(comment);
        if (a) {
            return ResultUtil.success("评论提交成功,系统正在审核");
        } else {
            return ResultUtil.error("评论提交失败");
        }
    }

    /**
     * 文章访问
     * @param request 请求
     * @param articleId 文章编号
     * @return
     */
    @PostMapping("article/visit")
    public ResponseVo checkLook(HttpServletRequest request, Integer articleId) {
        /*浏览次数*/
        Date date = new Date();
        Long ip = NetUtil.ipv4ToLong(IpUtil.getIpStr(request));
        int checkCount = (int) articleVisitService.count(
                new QueryWrapper<ArticleVisit>().eq("article_id", articleId)
                        .eq("user_ip", ip).gt("create_time", DateUtil.offsetHour(date, -1)));
        if (checkCount == 0) {
            ArticleVisit articleVisit = new ArticleVisit();
            articleVisit.setArticleId(articleId);
            articleVisit.setUserIp(ip);
            articleVisit.setVisitTime(date);
            articleVisitService.save(articleVisit);
            return ResultUtil.success("浏览次数+1");
        } else {
            return ResultUtil.error("一小时内重复浏览不增加次数哦");
        }
    }

    /**
     * 点赞
     * @param request 请求
     * @param articleId 文章编号
     * @return
     */
    @PostMapping("article/like")
    public ResponseVo love(HttpServletRequest request, Integer articleId) {
        Date date = new Date();
        Long ip = NetUtil.ipv4ToLong(IpUtil.getIpStr(request));

        long likeCount = articleLikeService.count(new QueryWrapper<ArticleLike>()
                .eq("article_id", articleId).eq("user_ip", ip));

        if (likeCount == 0L) {
            ArticleLike articleLike = new ArticleLike();
            articleLike.setArticleId(articleId);
            articleLike.setUserIp(ip);
            articleLike.setLikeTime(date);
            articleLikeService.save(articleLike);
            return ResultUtil.success("点赞成功");
        } else {
            return ResultUtil.error("您已赞过了哦~");
        }

    }

}
