package com.loncol.cms.controller.admin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.common.shiro.MyShiroRealm;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.Permission;
import com.loncol.cms.model.po.Role;
import com.loncol.cms.model.po.User;
import com.loncol.cms.service.PermissionService;
import com.loncol.cms.service.RoleService;
import com.loncol.cms.model.vo.PageResultVo;
import com.loncol.cms.model.vo.PermissionTreeListVo;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 后台角色配置
 * @author Hardy
 * @date 2022-05-11
 */
@Slf4j
@Controller
@AllArgsConstructor
public class RoleController {

    private final RoleService roleService;
    private final PermissionService permissionService;
    private final MyShiroRealm myShiroRealm;


    @PostMapping("/role/list")
    @ResponseBody
    public PageResultVo pageRoles(Role roleQuery, Integer pageNum, Integer pageSize) {
        IPage<Role> rolePage = roleService.listByCondition(roleQuery, pageNum, pageSize);
        return ResultUtil.table(rolePage.getRecords(), rolePage.getTotal());

    }

    @GetMapping("/role/add")
    public String add() {
        return CoreConst.ADMIN_PREFIX + "role/form";
    }

    /**
     * 新增角色
     * @param role 角色
     * @return Response对象
     */
    @PostMapping("/role/add")
    @ResponseBody
    public ResponseVo addRole(Role role) {
        return roleService.save(role)
                ? ResultUtil.success("添加角色成功")
                : ResultUtil.error("添加角色失败");
    }

    /**
     * 删除角色
     * @param id 编号
     * @return Response对象
     */
    @PostMapping("/role/delete")
    @ResponseBody
    public ResponseVo deleteRole(Integer id) {
        if (CollUtil.isNotEmpty(roleService.findUserIdsByRoleId(id))) {
            return ResultUtil.error("删除失败,该角色下存在用户");
        }
        return roleService.updateStatus(id, CoreConst.STATUS_INVALID) > 0
                ? ResultUtil.success("删除角色成功")
                : ResultUtil.error("删除角色失败");
    }

    /**
     * 批量删除角色
     * @param ids 编号列表
     * @return Response对象
     */
    @PostMapping("/role/batch/delete")
    @ResponseBody
    public ResponseVo batchDeleteRole(@RequestParam("ids[]") Integer[] ids) {
        List<Integer> roleIdsList = Arrays.asList(ids);
        if (CollUtil.isNotEmpty(roleService.findUserIdsByRoleIds(roleIdsList))) {
            return ResultUtil.error("删除失败,选择的角色下存在用户");
        }
        return roleService.updateStatusBatch(roleIdsList, CoreConst.STATUS_INVALID) > 0?
            ResultUtil.success("删除角色成功"):ResultUtil.error("删除角色失败");
    }

    /**
     * 编辑角色详情
     * @param model 模块
     * @param id 编号
     * @return 详情字符串
     */
    @GetMapping("/role/edit")
    public String detail(Model model, Integer id) {
        Role role = roleService.getById(id);
        model.addAttribute("role", role);
        return CoreConst.ADMIN_PREFIX + "role/form";
    }

    /**
     * 编辑角色
     * @param role 角色
     * @return Response对象
     */
    @PostMapping("/role/edit")
    @ResponseBody
    public ResponseVo editRole(@ModelAttribute("role") Role role) {
        return roleService.updateById(role)
                ?ResultUtil.success("编辑角色成功")
                :ResultUtil.error("编辑角色失败");
    }

    /**
     * 分配权限列表查询
     * TODO:后续分析
     * @param id 编号
     * @return 权限列表
     */
    @PostMapping("/role/assign/permission/list")
    @ResponseBody
    public List<PermissionTreeListVo> assignRole(Integer id) {
        List<PermissionTreeListVo> listVos = new ArrayList<>();
        List<Permission> allPermissions = permissionService.selectAll(CoreConst.STATUS_VALID);
        List<Permission> hasPermissions = roleService.findPermsByRoleId(id);
        for (Permission permission : allPermissions) {
            PermissionTreeListVo vo = new PermissionTreeListVo();
            vo.setId(permission.getId());
            vo.setPermName(permission.getPermName());
            vo.setParentId(permission.getParentId());
            for (Permission hasPermission : hasPermissions) {
                if (hasPermission.getId().equals(permission.getId())) {
                    vo.setChecked(true);
                    break;
                }
            }
            listVos.add(vo);
        }
        return listVos;
    }


    /**
     * 分配权限
     * @param id 编号
     * @param permIdStr 权限
     * @return Response对象
     */
    @PostMapping("/role/assign/permission")
    @ResponseBody
    public ResponseVo assignRole(Integer id, String permIdStr) {
        List<Integer> permIdList = new ArrayList<>();
        if (StrUtil.isNotBlank(permIdStr)) {
            permIdList = Arrays.asList(Convert.toIntArray(permIdStr.split(",")));
        }

        try {
            roleService.addAssignPermission(id, permIdList);
            List<User> userList = roleService.findUsersByRoleId(id);
            if (!userList.isEmpty()) {
                List<String> userIdList = new ArrayList<>();
                for (User user : userList) {
                    userIdList.add(user.getId().toString());
                }
                myShiroRealm.clearAuthorizationByUserId(userIdList);
            }
            return ResultUtil.success("分配权限成功");
        } catch (Exception e) {
            return ResultUtil.error("分配权限失败");
        }
    }

}
