package com.loncol.cms.controller.admin;

import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.service.OssService;
import com.loncol.cms.model.vo.UploadResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * 后台文件上传接口、云存储配置
 * @author Hardy
 * @date 2022-05-11
 */
@Slf4j
@Controller
@AllArgsConstructor
public class UploadController {

    private final OssService ossService;

    @ResponseBody
    @PostMapping("/attachment/upload")
    public UploadResponse upload(@RequestParam(value = "file", required = false) MultipartFile file) {
        return ossService.upload(file);
    }

    @ResponseBody
    @PostMapping("/attachment/uploadForCkEditor")
    public Object upload2QiniuForMd(@RequestParam("upload") MultipartFile file) {
        Map<String, Object> resultMap = new HashMap<>(2);
        UploadResponse responseVo = upload(file);
        if (CoreConst.SUCCESS_CODE.equals(responseVo.getStatus())) {
            resultMap.put("uploaded", 1);
            resultMap.put("url", responseVo.getUrl());
            return resultMap;
        }
        return resultMap;
    }
}
