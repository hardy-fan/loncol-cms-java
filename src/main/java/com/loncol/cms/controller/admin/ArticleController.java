package com.loncol.cms.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.vo.PaginationVo;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.*;
import com.loncol.cms.service.*;
import com.loncol.cms.model.vo.ArticleConditionVo;
import com.loncol.cms.model.vo.PageResultVo;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.apache.shiro.SecurityUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 文章管理
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class ArticleController {

    private final ArticleService articleService;
    private final ArticleContentService articleContentService;
    private final ArticleTagService articleTagService;
    private final CategoryService categoryService;
    private final TagService tagService;

    /**
     * 文章列表
     * @param vo
     * @param pageNum
     * @param pageSize
     * @return
     */
    @PostMapping("article/list")
    @ResponseBody
    public PageResultVo loadArticle(ArticleConditionVo vo, Integer pageNum, Integer pageSize) {
        IPage<Article> page = new PaginationVo<>(pageNum, pageSize);
        List<Article> articleList = articleService.findByCondition(page, vo);
        return ResultUtil.table(articleList, page.getTotal());
    }

    /**
     * 添加文章
     * @param model
     * @return
     */
    @GetMapping("article/add")
    public String addArticle(Model model) {
        List<Category> categories = categoryService.selectByTypeId(3);
        List<Tag> tags = tagService.list();
        model.addAttribute("categories", categories);
        model.addAttribute("tags", tags);
        Article article = new Article()
                .setTags(new ArrayList<>())
                .setRecommentFlag(0)
                .setCommentFlag(1);
        model.addAttribute("article", article);
        return CoreConst.ADMIN_PREFIX + "article/publish";
    }

    /**
     * 添加文章
     * @param article
     * @param tags
     * @return
     */
    @PostMapping("article/add")
    @ResponseBody
    @Transactional
    @CacheEvict(value = "article", allEntries = true)
    public ResponseVo add(Article article, Integer[] tags) {
        try {
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            article.setCreateUser(user.getId());
            article.setAuthor(user.getNickname());
            Date date = new Date();
            article.setCreateTime(date);
            article.setUpdateTime(date);

            articleService.save(article);
            articleTagService.insertList(Arrays.asList(tags), article.getId());

            ArticleContent articleContent = new ArticleContent();
            articleContent.setArticleId(article.getId());
            articleContent.setContent(article.getContent());
            articleContentService.save(articleContent);

            return ResultUtil.success("保存文章成功");
        } catch (Exception e) {
            return ResultUtil.error("保存文章失败");
        }
    }

    /**
     * 编辑文章页面
     * @param model
     * @param id
     * @return
     */
    @GetMapping("article/edit")
    public String edit(Model model, Integer id) {
        Article article = articleService.getDetailById(id);
        model.addAttribute("article", article);
        List<Category> categories = categoryService.selectByTypeId(3);
        model.addAttribute("categories", categories);
        List<Tag> tags = tagService.list();
        model.addAttribute("tags", tags);
        return CoreConst.ADMIN_PREFIX + "article/publish";
    }

    /**
     * 编辑文章动作
     * @param article
     * @param tag
     * @return
     */
    @PostMapping("article/edit")
    @ResponseBody
    @CacheEvict(value = "article", allEntries = true)
    public ResponseVo edit(Article article, Integer[] tag) {
        article.setUpdateTime(new Date());
        articleService.updateById(article);
        List<Integer> tagList = tag != null ? Arrays.asList(tag): null;
        //设置标签
        articleService.addAssignTags(article.getId(), tagList);
        //设置内容
        articleService.assignContent(article.getId(), article.getContent());
        return ResultUtil.success("编辑文章成功");
    }

    /**
     * 文章删除
     * @param id
     * @return
     */
    @PostMapping("article/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        return deleteBatch(new Integer[]{id});
    }

    /**
     * 文章批量删除
     * @param ids
     * @return
     */
    @PostMapping("article/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        return articleService.removeBatchByIds(Arrays.asList(ids))
                ?ResultUtil.success("删除文章成功")
                :ResultUtil.error("删除文章失败");
    }
}
