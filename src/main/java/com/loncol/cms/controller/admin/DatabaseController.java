package com.loncol.cms.controller.admin;

import com.loncol.cms.common.util.CoreConst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 后台SQL监控
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
public class DatabaseController {

    @GetMapping(value = "/database/monitoring")
    public ModelAndView databaseMonitoring() {
        return new ModelAndView(CoreConst.ADMIN_PREFIX + "database/monitoring");
    }
}
