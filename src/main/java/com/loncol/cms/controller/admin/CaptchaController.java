package com.loncol.cms.controller.admin;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.script.ScriptUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 获取验证码图片
 * @author Hardy
 * @date 2022-05-11
 */
@Slf4j
@Controller
@AllArgsConstructor
public class CaptchaController {

    /**
     * 获取验证码图片
     * @param request  the request
     * @param response the response
     */
    @RequestMapping("/verificationCode")
    public void getCaptchaCode(HttpServletRequest request,
                               HttpServletResponse response,
                               HttpSession session) {

        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(130, 48, 2, 4);
        captcha.setGenerator(new MathGenerator());
        String strCode = ScriptUtil.eval(captcha.getCode().replace("=", "")).toString();
        session.setAttribute("code", strCode);

        try {
            ServletOutputStream outputStream = response.getOutputStream();
            captcha.write(outputStream);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
