package com.loncol.cms.controller.admin;

import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.service.UserService;
import com.loncol.cms.model.vo.PageResultVo;
import com.loncol.cms.model.vo.ResponseVo;
import com.loncol.cms.model.vo.UserOnlineVo;
import com.loncol.cms.model.vo.UserSessionVo;
import lombok.AllArgsConstructor;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;

/**
 * 后台在线用户管理
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class OnlineUserController {

    private final UserService userService;

    /**
     * 在线用户列表
     * @param user 在线用户
     * @param pageNum 页码
     * @param pageSize 每页条数
     * @return
     */
    @PostMapping("/online/user/list")
    @ResponseBody
    public PageResultVo onlineUsers(UserOnlineVo user, Integer pageNum, Integer pageSize) {
        List<UserOnlineVo> userList = userService.selectOnlineUsers(user);
        int endIndex = Math.min(pageNum * pageSize, userList.size());
        return ResultUtil.table(userList.subList((pageNum - 1) * pageSize, endIndex), (long) userList.size());
    }

    /**
     * 强制踢出用户
     * @param sessionId 会话编号
     * @param username 用户名
     * @return
     */
    @PostMapping("/online/user/kickout")
    @ResponseBody
    public ResponseVo kickout(String sessionId, String username) {
        try {
            if (SecurityUtils.getSubject().getSession().getId().equals(sessionId)) {
                return ResultUtil.error("不能踢出自己");
            }
            userService.kickout(sessionId, username);
            return ResultUtil.success("踢出用户成功");
        } catch (Exception e) {
            return ResultUtil.error("踢出用户失败");
        }
    }

    /**
     * 批量强制踢出用户
     * @param sessions 会话
     * @return
     */
    @PostMapping("/online/user/batch/kickout")
    @ResponseBody
    public ResponseVo kickout(@RequestBody List<UserSessionVo> sessions) {
        try {
            boolean hasOwn = false;
            Serializable sessionId = SecurityUtils.getSubject().getSession().getId();
            for (UserSessionVo sessionVo : sessions) {
                if (sessionVo.getSessionId().equals(sessionId)) {
                    hasOwn = true;
                } else {
                    userService.kickout(sessionVo.getSessionId(), sessionVo.getUsername());
                }
            }
            if (hasOwn) {
                return ResultUtil.success("不能踢出自己");
            }
            return ResultUtil.success("踢出用户成功");
        } catch (Exception e) {
            return ResultUtil.error("踢出用户失败");
        }
    }
}
