package com.loncol.cms.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.InfoPosition;
import com.loncol.cms.service.InfoPositionService;
import com.loncol.cms.model.vo.PageResultVo;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

/**
 * 后台标签配置
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class InfoPositionController {

    private final InfoPositionService positionService;

    @GetMapping("position/add")
    public String add() {
        return CoreConst.ADMIN_PREFIX + "position/form";
    }

    @PostMapping("position/list")
    @ResponseBody
    public PageResultVo loadTags(InfoPosition query, Integer pageNum, Integer pageSize) {
        IPage<InfoPosition> positionIPage = positionService.listByCondition(query, pageNum, pageSize );
        return ResultUtil.table(positionIPage.getRecords(), positionIPage.getTotal());
    }

    @PostMapping("position/add")
    @ResponseBody
    @CacheEvict(value = "position", allEntries = true)
    public ResponseVo add(InfoPosition position) {
        return positionService.save(position)
                ? ResultUtil.success("新增标签成功")
                : ResultUtil.error("新增标签失败");
    }

    @GetMapping("position/edit")
    public String edit(Model model, Integer id) {
        InfoPosition position = positionService.getById(id);
        model.addAttribute("position", position);
        return CoreConst.ADMIN_PREFIX + "position/form";
    }

    @PostMapping("position/edit")
    @ResponseBody
    @CacheEvict(value = "position", allEntries = true)
    public ResponseVo edit(InfoPosition position) {
        return positionService.updateById(position)
                ?ResultUtil.success("编辑标签成功")
                :ResultUtil.error("编辑标签失败");
    }

    @PostMapping("position/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        return deleteBatch(new Integer[]{id});
    }

    @PostMapping("position/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        return positionService.removeBatchByIds(Arrays.asList(ids))
                ?ResultUtil.success("删除标签成功")
                :ResultUtil.error("删除标签失败");
    }
}
