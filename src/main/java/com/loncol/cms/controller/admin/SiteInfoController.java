package com.loncol.cms.controller.admin;

import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.service.SiteConfigService;
import com.loncol.cms.service.StaticHtmlService;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * 后台网站信息配置
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class SiteInfoController {

    private final SiteConfigService configService;
    private final StaticHtmlService staticHtmlService;

    @PostMapping("/siteinfo/edit")
    @ResponseBody
    public ResponseVo save(@RequestParam Map<String, String> map, HttpServletRequest request, HttpServletResponse response) {
        try {
            if (map.containsKey(CoreConst.SITE_STATIC_KEY)) {
                boolean siteStaticOn = "on".equalsIgnoreCase(map.get(CoreConst.SITE_STATIC_KEY));
                if (siteStaticOn) {
                    staticHtmlService.makeStaticSite(request, response, true);
                }
                CoreConst.SITE_STATIC.set(siteStaticOn);
            }
            configService.updateAll(map, request, response);
            return ResultUtil.success("保存网站信息成功");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultUtil.error("保存网站信息失败");
        }
    }
}
