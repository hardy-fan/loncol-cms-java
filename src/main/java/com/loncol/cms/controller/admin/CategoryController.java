package com.loncol.cms.controller.admin;

import cn.hutool.core.collection.CollUtil;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.Article;
import com.loncol.cms.model.po.Category;
import com.loncol.cms.model.po.DictData;
import com.loncol.cms.service.ArticleService;
import com.loncol.cms.service.CategoryService;
import com.loncol.cms.service.DictDataService;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.util.ListUtils;

import java.util.Arrays;
import java.util.List;

/**
 * 后台类目管理
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;
    private final ArticleService articleService;
    private final DictDataService dictDataService;

    @PostMapping("category/list")
    @ResponseBody
    public List<Category> loadCategory(boolean isFistLevel) {
        Category categoryCondition = new Category();
        categoryCondition.setStatus(CoreConst.STATUS_VALID);
        if (isFistLevel) {
            categoryCondition.setParentId(CoreConst.TOP_MENU_ID);
        }
        List<Category> categories = categoryService.selectCategories(categoryCondition);
        return categories;
    }

    @GetMapping("category/add")
    public String add(Model model) {
        Category category = new Category();
        category.setSort(255);
        List<DictData> dictDataList = dictDataService.selectDictDataList(4);
        model.addAttribute("category", category);
        model.addAttribute("categoryTypes", dictDataList);
        return CoreConst.ADMIN_PREFIX + "category/form";
    }

    @PostMapping("category/add")
    @ResponseBody
    @CacheEvict(value = "category", allEntries = true)
    public ResponseVo add(Category category) {
        if (!CoreConst.TOP_MENU_ID.equals(category.getParentId())) {
            List<Article> Articles = articleService.selectByCategoryId(category.getParentId());
            if (!ListUtils.isEmpty(Articles)) {
                return ResultUtil.error("添加失败，父级分类下存在文章");
            }
        }
        category.setStatus(CoreConst.STATUS_VALID);
        setCategory(category);
        boolean i = categoryService.save(category);
        if (i) {
            return ResultUtil.success("新增分类成功");
        } else {
            return ResultUtil.error("新增分类失败");
        }
    }

    @GetMapping("category/edit")
    public String edit(Model model, Integer id) {
        Category category = categoryService.getDetailById(id);
        List<DictData> dictDataList = dictDataService.selectDictDataList(4);
        model.addAttribute("category", category);
        model.addAttribute("categoryTypes", dictDataList);
        return CoreConst.ADMIN_PREFIX + "category/form";
    }

    @PostMapping("category/edit")
    @ResponseBody
    @CacheEvict(value = "category", allEntries = true)
    public ResponseVo edit(Category category) {
        setCategory(category);
        return categoryService.updateById(category)
                ?ResultUtil.success("编辑分类成功")
                :ResultUtil.error("编辑分类失败");
    }

    private void setCategory(Category category){
        if(category.getSort() == null){
            category.setSort(255);
        }
        if(category.getParentId() != null && category.getParentId()>0){
            Category category1 = categoryService.getById(category.getParentId());
            category.setParentPath(category1.getParentPath()+category.getParentId()+",");
        }
    }

    @PostMapping("category/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        if (CollUtil.isNotEmpty(categoryService.selectByPid(id))) {
            return ResultUtil.error("该分类下存在子分类！");
        }
        return categoryService.removeBatchByIds(Arrays.asList(new Integer[] {id}))
                ?ResultUtil.success("删除分类成功")
                :ResultUtil.error("删除分类失败");
    }

    @PostMapping("category/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        if (CollUtil.isNotEmpty(categoryService.selectByPids(ids))) {
            return ResultUtil.error("待删除的分类下存在子分类！");
        }
        return categoryService.removeBatchByIds(Arrays.asList(ids))
                ?ResultUtil.success("删除分类成功")
                :ResultUtil.error("删除分类失败");
    }
}
