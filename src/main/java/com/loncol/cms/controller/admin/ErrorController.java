package com.loncol.cms.controller.admin;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 统一错误跳转页面，如403，404，500
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class ErrorController {

    /**
     * shiro无权限时进入
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping("/error/403")
    public String noPermission(HttpServletRequest request, HttpServletResponse response, Model model) {
        response.setStatus(HttpStatus.FORBIDDEN.value());
        return "error/403";
    }

    @RequestMapping("/error/404")
    public String notFund(HttpServletRequest request, HttpServletResponse response, Model model) {
        return "error/404";
    }

    @RequestMapping("/error/500")
    public String sysError(HttpServletRequest request, HttpServletResponse response, Model model) {
        return "error/500";
    }

}
