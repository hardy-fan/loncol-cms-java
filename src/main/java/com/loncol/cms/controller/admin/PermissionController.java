package com.loncol.cms.controller.admin;

import cn.hutool.core.util.StrUtil;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.common.shiro.ShiroService;
import com.loncol.cms.model.po.Permission;
import com.loncol.cms.service.PermissionService;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.List;

/**
 * 后台权限配置管理
 * @author Hardy
 * @date 2022-05-11
 */
@Slf4j
@Controller
@AllArgsConstructor
public class PermissionController {

    /**
     * 1:全部资源，2：菜单资源
     */
    private static final String[] MENU_FLAG = {"1", "2"};

    private final PermissionService permissionService;
    private final ShiroService shiroService;

    /**
     * 添加权限页面
     * @param model
     * @return
     */
    @GetMapping("/permission/add")
    public String add(Model model) {
        model.addAttribute("permission", new Permission().setPermType("0"));
        return CoreConst.ADMIN_PREFIX + "permission/form";
    }

    /**
     * 权限列表
     * @param flag
     * @return
     */
    @PostMapping("/permission/list")
    @ResponseBody
    public List<Permission> loadPermissions(String flag) {
        List<Permission> permissionList = new ArrayList<Permission>();
        if (StrUtil.isBlank(flag) || MENU_FLAG[0].equals(flag)) {
            permissionList = permissionService.selectAll(CoreConst.STATUS_VALID);
        } else if (MENU_FLAG[1].equals(flag)) {
            permissionList = permissionService.selectAllMenuName(CoreConst.STATUS_VALID);
        }
        return permissionList;
    }

    /**
     * 添加权限操作
     * @param permission
     * @return
     */
    @ResponseBody
    @PostMapping("/permission/add")
    public ResponseVo addPermission(Permission permission) {
        boolean result = permissionService.save(permission);
        if (result) {
            shiroService.updatePermission();
            return ResultUtil.success("添加权限成功");
        } else {
            return ResultUtil.error("添加权限失败");
        }

    }

    /**
     * 删除权限操作
     * @param id
     * @return
     */
    @ResponseBody
    @PostMapping("/permission/delete")
    public ResponseVo deletePermission(Integer id) {
        Long subPermsByPermissionIdCount = permissionService.getSubCountByPermId(id);
        if (subPermsByPermissionIdCount > 0) {
            return ResultUtil.error("该资源存在下级资源，无法删除！");
        }
        boolean result = permissionService.updateStatus(id, CoreConst.STATUS_INVALID);
        if (result) {
            shiroService.updatePermission();
            return ResultUtil.success("删除权限成功");
        } else {
            return ResultUtil.error("删除权限失败");
        }
    }

    /**
     * 权限详情
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/permission/edit")
    public ModelAndView detail(Model model, Integer id) {
        ModelAndView modelAndView = new ModelAndView();
        Permission permission = permissionService.getById(id);
        if (null != permission) {
            if (CoreConst.TOP_MENU_ID.equals(permission.getParentId())) {
                model.addAttribute("parentName", CoreConst.TOP_MENU_NAME);
            } else {
                Permission parent = permissionService.getById(permission.getParentId());
                if (parent != null) {
                    model.addAttribute("parentName", parent.getPermName());
                }
            }
            model.addAttribute("permission", permission);
            modelAndView.setViewName(CoreConst.ADMIN_PREFIX + "permission/form");
        } else {
            log.error("根据权限id获取权限详情失败，权限id: {}", id);
            modelAndView.setView(new RedirectView("/error/500", true, false));
        }
        return modelAndView;
    }

    /**
     * 编辑权限
     * @param permission
     * @return
     */
    @ResponseBody
    @PostMapping("/permission/edit")
    public ResponseVo editPermission(@ModelAttribute("permission") Permission permission) {
        boolean result = permissionService.updateById(permission);
        if (result) {
            shiroService.updatePermission();
            return ResultUtil.success("编辑权限成功");
        } else {
            return ResultUtil.error("编辑权限失败");
        }
    }

}
