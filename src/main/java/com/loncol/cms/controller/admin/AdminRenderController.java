package com.loncol.cms.controller.admin;

import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.po.Category;
import com.loncol.cms.model.po.User;
import com.loncol.cms.service.CategoryService;
import com.loncol.cms.service.PermissionService;
import com.loncol.cms.service.SiteConfigService;
import com.loncol.cms.service.SiteStatService;
import lombok.AllArgsConstructor;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

/**
 * 后台管理页面跳转控制器
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class AdminRenderController {

    private final CategoryService categoryService;
    private final SiteConfigService configService;
    private final SiteStatService statService;
    private final PermissionService permissionService;

    /**
     * 后台首页
     */
    @RequestMapping("/admin")
    public String index(Model model) {
        User loginUser = (User) SecurityUtils.getSubject().getPrincipal();
        Integer userId = ("admin".equals(loginUser.getUsername()))? 0: loginUser.getId();

        model.addAttribute("menuTree",
                permissionService.selectMenuTreeByUserId(userId));
        return CoreConst.ADMIN_PREFIX + "index/index";
    }

    /**
     * 工作台
     */
    @GetMapping("/workdest")
    public String workdest(Model model) {
        model.addAttribute("statistic", statService.indexStatistic());
        return CoreConst.ADMIN_PREFIX + "index/dashboard";
    }

    /**
     * 用户列表
     */
    @GetMapping("/users")
    public String userList() {
        return CoreConst.ADMIN_PREFIX + "user/list";
    }

    /**
     * 角色列表入口
     */
    @GetMapping("/roles")
    public String roleList() {
        return CoreConst.ADMIN_PREFIX + "role/list";
    }

    /**
     * 权限列表入口
     */
    @GetMapping("/permissions")
    public String permissionList() {
        return CoreConst.ADMIN_PREFIX + "permission/list";
    }

    /**
     * 在线用户入口
     */
    @GetMapping("/online/users")
    public String onlineUsers() {
        return CoreConst.ADMIN_PREFIX + "onlineUsers/list";
    }

    /**
     * 网站基本信息
     *
     * @param model
     */
    @GetMapping("/siteinfo")
    public String siteinfo(Model model) {
        Map<String, String> map = configService.selectAll();
        model.addAttribute("siteinfo", map);
        return CoreConst.ADMIN_PREFIX + "site/siteinfo";
    }

    /**
     * 分类
     */
    @GetMapping("/categories")
    public String categories() {
        return CoreConst.ADMIN_PREFIX + "category/list";
    }

    /**
     * 标签
     */
    @GetMapping("/tags")
    public String tags() {
        return CoreConst.ADMIN_PREFIX + "tag/list";
    }

    /**
     * 推荐位
     */
    @GetMapping("/positions")
    public String positions() {
        return CoreConst.ADMIN_PREFIX + "position/list";
    }

    /**
     * 信息块
     */
    @GetMapping("/blocks")
    public String blocks() {
        return CoreConst.ADMIN_PREFIX + "block/list";
    }

    /**
     * 文章
     *
     * @param model
     */
    @GetMapping("/articles")
    public String articles(Model model) {
        List<Category> categories = categoryService.selectByTypeId(3);
        model.addAttribute("categories", categories);
        return CoreConst.ADMIN_PREFIX + "article/list";
    }

    /**
     * 单页
     */
    @GetMapping("/singles")
    public String links(Model model) {
        List<Category> categories = categoryService.selectByTypeId(2);
        model.addAttribute("categories", categories);
        return CoreConst.ADMIN_PREFIX + "single/list";
    }

    /**
     * 评论
     */
    @GetMapping("/comments")
    public String comments() {
        return CoreConst.ADMIN_PREFIX + "comment/list";
    }

    /**
     * 主题
     */
    @GetMapping("themes")
    public String themes() {
        return CoreConst.ADMIN_PREFIX + "theme/list";
    }

}
