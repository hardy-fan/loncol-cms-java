package com.loncol.cms.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.Tag;
import com.loncol.cms.service.TagService;
import com.loncol.cms.model.vo.PageResultVo;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 后台标签配置
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class TagController {

    private final TagService tagService;

    @GetMapping("tag/add")
    public String add() {
        return CoreConst.ADMIN_PREFIX + "tag/form";
    }

    @PostMapping("tag/list")
    @ResponseBody
    public PageResultVo loadTags(Tag tagQuery, Integer pageNum, Integer pageSize) {
        IPage<Tag> tagPage = tagService.listByCondition(tagQuery, pageNum, pageSize );
        return ResultUtil.table(tagPage.getRecords(), tagPage.getTotal());
    }

    @PostMapping("tag/add")
    @ResponseBody
    @CacheEvict(value = "tag", allEntries = true)
    public ResponseVo add(Tag tag) {
        return tagService.save(tag)
                ? ResultUtil.success("新增标签成功")
                : ResultUtil.error("新增标签失败");
    }

    @GetMapping("tag/edit")
    public String edit(Model model, Integer id) {
        Tag tag = tagService.getById(id);
        model.addAttribute("tag", tag);
        return CoreConst.ADMIN_PREFIX + "tag/form";
    }

    @PostMapping("tag/edit")
    @ResponseBody
    @CacheEvict(value = "tag", allEntries = true)
    public ResponseVo edit(Tag tag) {
        return tagService.updateById(tag)
                ?ResultUtil.success("编辑标签成功")
                :ResultUtil.error("编辑标签失败");
    }

    @PostMapping("tag/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        return deleteBatch(new Integer[]{id});
    }

    @PostMapping("/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        return tagService.removeBatchByIds(Arrays.asList(ids))
                ?ResultUtil.success("删除标签成功")
                :ResultUtil.error("删除标签失败");
    }
}
