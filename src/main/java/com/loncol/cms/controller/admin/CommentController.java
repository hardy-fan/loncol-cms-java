package com.loncol.cms.controller.admin;

import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.IpUtil;
import com.loncol.cms.model.vo.PaginationVo;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.Comment;
import com.loncol.cms.model.po.User;
import com.loncol.cms.service.CommentService;
import com.loncol.cms.model.vo.CommentConditionVo;
import com.loncol.cms.model.vo.PageResultVo;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 后台评论管理
 * @author Hardy
 * @date 2022-05-11
 */
@RestController
@AllArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping("comment/list")
    public PageResultVo loadNotify(CommentConditionVo vo, Integer pageNum, Integer pageSize) {
        IPage<Comment> commentList = commentService.selectComments(
                new PaginationVo<>(pageNum, pageSize), vo);
        return ResultUtil.table(commentList.getRecords(), commentList.getTotal());
    }

    @PostMapping("comment/reply")
    public ResponseVo edit(Comment comment) {
        completeComment(comment);
        boolean i = commentService.save(comment);
        if (i) {
            return ResultUtil.success("回复评论成功");
        } else {
            return ResultUtil.error("回复评论失败");
        }
    }

    @PostMapping("comment/delete")
    public ResponseVo delete(Integer id) {
        Integer[] ids = {id};
        return commentService.deleteBatch(ids)>0
                ? ResultUtil.success("删除评论成功")
                : ResultUtil.error("删除评论失败");
    }

    @PostMapping("comment/batch/delete")
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        return commentService.deleteBatch(ids)>0
                ? ResultUtil.success("删除评论成功")
                : ResultUtil.error("删除评论失败");
    }

    @PostMapping("comment/audit")
    public ResponseVo audit(Comment comment, String replyContent) {
        try {
            commentService.updateById(comment);
            if (StrUtil.isNotBlank(replyContent)) {
                Comment replyComment = new Comment();
                replyComment.setParentId(comment.getParentId());
                replyComment.setArticleId(comment.getArticleId());
                replyComment.setContent(replyContent);
                completeComment(replyComment);
                commentService.save(replyComment);
            }
            return ResultUtil.success("审核成功");
        } catch (Exception e) {
            return ResultUtil.success("审核失败");
        }
    }

    private void completeComment(Comment comment) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        comment.setUserId(user.getId());
        comment.setNickname(user.getNickname());
        comment.setEmail(user.getEmail());
        comment.setAvatar(user.getAvatar());
        comment.setUserIp(NetUtil.ipv4ToLong(IpUtil.getIpStr(request)));
        comment.setStatus(CoreConst.STATUS_VALID);
    }

}
