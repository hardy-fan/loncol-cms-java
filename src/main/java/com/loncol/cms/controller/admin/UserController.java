package com.loncol.cms.controller.admin;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.loncol.cms.common.shiro.MyShiroRealm;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.Role;
import com.loncol.cms.model.po.User;
import com.loncol.cms.service.RoleService;
import com.loncol.cms.service.UserService;
import com.loncol.cms.model.vo.ChangePasswordVo;
import com.loncol.cms.model.vo.PageResultVo;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 后台用户配置
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class UserController {

    private final MyShiroRealm myShiroRealm;
    private final UserService userService;
    private final RoleService roleService;
    private final MyShiroRealm shiroRealm;

    /**
     * 用户列表数据
     */
    @PostMapping("/user/list")
    @ResponseBody
    public PageResultVo loadUsers(User userQuery, Integer pageNum, Integer pageSize) {
        IPage<User> tagPage = userService.listByCondition(userQuery, pageNum, pageSize);
        return ResultUtil.table(tagPage.getRecords(), tagPage.getTotal());
    }

    @GetMapping("/user/add")
    public String add() {
        return CoreConst.ADMIN_PREFIX + "user/form";
    }

    @GetMapping("/user/changePassword")
    public String changePassword() {
        return CoreConst.ADMIN_PREFIX + "user/changePassword";
    }

    /**
     * 新增用户
     */
    @PostMapping("/user/add")
    @ResponseBody
    public ResponseVo add(User userForm, String confirmPassword) {
        String username = userForm.getUsername();
        User user = userService.selectByUsername(username);
        if (null != user) {
            return ResultUtil.error("用户名已存在");
        }
        String password = userForm.getPassword();
        if (confirmPassword != null && password != null) {
            if (!confirmPassword.equals(password)) {
                return ResultUtil.error("两次密码不一致");
            }
        }
        userForm.setStatus(CoreConst.STATUS_VALID);
        Date date = new Date();
        userForm.setCreateTime(date);
        userForm.setUpdateTime(date);
        userService.encryptPassword(userForm);
        boolean result = userService.save(userForm);

        return (result)
                ? ResultUtil.success("添加用户成功")
                : ResultUtil.error("添加用户失败");
    }

    /**
     * 编辑用户表单
     */
    @GetMapping("/user/edit")
    public String userDetail(Model model, String id) {
        User user = userService.getById(id);
        model.addAttribute("user", user);
        return CoreConst.ADMIN_PREFIX + "user/form";
    }

    /**
     * 编辑用户
     */
    @PostMapping("/user/edit")
    @ResponseBody
    public ResponseVo editUser(User userForm) {
        Assert.notNull(userForm, "param: user is null");
        userForm.setUpdateTime(new Date());
        boolean result = userService.updateById(userForm);
        return (result)
                ? ResultUtil.success("编辑用户成功！")
                : ResultUtil.error("编辑用户失败");
    }

    /**
     * 删除用户
     */
    @PostMapping("/user/delete")
    @ResponseBody
    public ResponseVo deleteUser(Integer id) {
        return userService.updateStatus(id, CoreConst.STATUS_INVALID)?
                ResultUtil.success("删除用户成功"):ResultUtil.error("删除用户失败");
    }

    /**
     * 批量删除用户
     */
    @PostMapping("/user/batch/delete")
    @ResponseBody
    public ResponseVo batchDeleteUser(@RequestParam("ids[]") Integer[] ids) {
        List<Integer> idList = Arrays.asList(ids);
        return userService.updateStatusBatch(idList, CoreConst.STATUS_INVALID)?
                ResultUtil.success("删除用户成功"):
                ResultUtil.error("删除用户失败");
    }

    /**
     * 分配角色列表查询
     */
    @PostMapping("/user/assign/role/list")
    @ResponseBody
    public Map<String, Object> assignRoleList(Integer id) {
        List<Role> roleList = roleService.list(Wrappers.<Role>lambdaQuery().eq(Role::getStatus, 1));
        Set<Integer> hasRoles = roleService.findRoleIdsByUserId(id);
        Map<String, Object> jsonMap = new HashMap<>(2);
        jsonMap.put("rows", roleList);
        jsonMap.put("hasRoles", hasRoles);
        return jsonMap;
    }

    /**
     * 保存分配角色
     */
    @PostMapping("/user/assign/role")
    @ResponseBody
    public ResponseVo assignRole(Integer id, String roleIdStr) {
        ResponseVo responseVo;
        String[] roleIds = roleIdStr.split(",");
        List<Integer> roleIdsList = Arrays.stream(roleIds)
                .map(role -> Integer.parseInt(role.toString()))
                .collect(Collectors.toList());
        try {
            userService.addAssignRole(id, roleIdsList);
            myShiroRealm.clearAuthorizationByUserId(Collections.singletonList(id.toString()));
            responseVo = ResultUtil.success("分配角色成功");
        } catch (Exception e) {
            responseVo = ResultUtil.error("分配角色失败");
        }
        return responseVo;
    }

    /**
     * 修改密码
     * @param changePasswordVo
     * @return
     */
    @RequestMapping(value = "/user/changePassword", method = RequestMethod.POST)
    @ResponseBody
    public ResponseVo changePassword(ChangePasswordVo changePasswordVo) {
        if (!changePasswordVo.getNewPassword().equals(changePasswordVo.getConfirmNewPassword())) {
            return ResultUtil.error("两次密码输入不一致");
        }
        User loginUser = userService.getById(((User) SecurityUtils.getSubject().getPrincipal()).getId());
        User newUser = ObjectUtil.clone(loginUser);
        String sysOldPassword = loginUser.getPassword();
        newUser.setPassword(changePasswordVo.getOldPassword());
        String entryOldPassword = userService.getEncryptPassword(newUser);
        if (sysOldPassword.equals(entryOldPassword)) {
            newUser.setPassword(changePasswordVo.getNewPassword());
            userService.encryptPassword(newUser);
            userService.updateById(newUser);
            List<String> userIds = new ArrayList<>();
            userIds.add(loginUser.getId().toString());
            shiroRealm.removeCachedAuthenticationInfo(userIds);
        } else {
            return ResultUtil.error("旧密码输入有误");
        }
        return ResultUtil.success("修改密码成功");
    }

}
