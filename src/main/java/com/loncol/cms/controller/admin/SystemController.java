package com.loncol.cms.controller.admin;

import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.Category;
import com.loncol.cms.model.po.Permission;
import com.loncol.cms.model.po.User;
import com.loncol.cms.model.vo.ResponseVo;
import com.loncol.cms.service.CategoryService;
import com.loncol.cms.service.PermissionService;
import com.loncol.cms.service.SiteConfigService;
import com.loncol.cms.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 后台首页、登录等接口
 * @author Hardy
 * @date 2022-05-11
 */
@Slf4j
@Controller
@AllArgsConstructor
public class SystemController {

    private final UserService userService;
    private final PermissionService permissionService;
    private final CategoryService categoryService;
    private final SiteConfigService configService;

    /**
     * 访问登录
     * @param model
     */
    @GetMapping("/login")
    public ModelAndView login(Model model) {
        ModelAndView modelAndView = new ModelAndView();
        if (SecurityUtils.getSubject().isAuthenticated()) {
            modelAndView.setView(new RedirectView("/admin", true, false));
            return modelAndView;
        }
        Category category = new Category();
        category.setStatus(CoreConst.STATUS_VALID);
        // TODO:?
        model.addAttribute("categoryList", categoryService.selectCategories(category));
        getSysConfig(model);
        modelAndView.setViewName("system/login");
        return modelAndView;
    }

    /**
     * 提交登录
     * @param request
     * @param username
     * @param password
     * @param verification
     * @param rememberMe
     * @return
     */
    @PostMapping("/login")
    @ResponseBody
    public ResponseVo login(HttpServletRequest request, HttpSession session,
                            String username,
                            String password,
                            String verification,
                            @RequestParam(value = "rememberMe", defaultValue = "0") Integer rememberMe) {
        // 验证码验证
        String sessionCheckCode = (String) session.getAttribute("code");
        if (verification == null || !sessionCheckCode.equals(verification) ){
            session.removeAttribute("code");
            return ResultUtil.error("验证码错误！");
        }
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            token.setRememberMe(1 == rememberMe);
            Subject subject = SecurityUtils.getSubject();
            subject.login(token);
        } catch (LockedAccountException e) {
            token.clear();
            return ResultUtil.error("用户已经被锁定不能登录，请联系管理员！");
        } catch (AuthenticationException e) {
            token.clear();
            return ResultUtil.error("用户名或者密码错误！");
        }
        //更新最后登录时间
        userService.updateLastLogin((User) SecurityUtils.getSubject().getPrincipal());
        return ResultUtil.success("登录成功！");
    }

    /**
     * 踢出
     * @param model
     * @return
     */
    @GetMapping("/kickout")
    public String kickout(Model model) {
        Category category = new Category();
        category.setStatus(CoreConst.STATUS_VALID);
        model.addAttribute("categoryList", categoryService.selectCategories(category));
        getSysConfig(model);
        return "system/kickout";
    }

    /**
     * 登出
     * @return
     */
    @RequestMapping("/logout")
    public ModelAndView logout() {
        Subject subject = SecurityUtils.getSubject();
        if (null != subject) {
            String username = ((User) SecurityUtils.getSubject().getPrincipal()).getUsername();
            Serializable sessionId = SecurityUtils.getSubject().getSession().getId();
            userService.kickout(sessionId, username);
        }
        subject.logout();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(new RedirectView("/admin", true, false));
        return modelAndView;
    }

    /**
     * 获取当前登录用户的菜单
     * @return
     */
    @PostMapping("/menu")
    @ResponseBody
    public List<Permission> getMenus() {
        return permissionService.selectMenuByUserId(((User) SecurityUtils.getSubject().getPrincipal()).getId());
    }

    private void getSysConfig(Model model) {
        Map<String, String> map = configService.selectAll();
        model.addAttribute("sysConfig", map);
        Category category = new Category();
        category.setStatus(CoreConst.STATUS_VALID);
        model.addAttribute("categoryList", categoryService.selectCategories(category));
    }
}
