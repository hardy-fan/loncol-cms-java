package com.loncol.cms.controller.admin;

import cn.hutool.core.collection.CollUtil;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.DictData;
import com.loncol.cms.model.po.InfoBlock;
import com.loncol.cms.model.po.InfoPosition;
import com.loncol.cms.service.DictDataService;
import com.loncol.cms.service.InfoBlockService;
import com.loncol.cms.service.InfoPositionService;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

/**
 * 信息块管理
 * @author Hardy
 * @date 2022-06-08
 */
@Controller
@AllArgsConstructor
public class InfoBlockController {
    private final InfoBlockService blockService;
    private final InfoPositionService positionService;
    private final DictDataService dictDataService;

    @PostMapping("block/list")
    @ResponseBody
    public List<InfoBlock> loadBlock(InfoBlock block) {
        return blockService.findByCondition(block);
    }

    @GetMapping("block/add")
    public String add(Model model) {
        List<InfoPosition> positions = positionService.list();
        model.addAttribute("positions", positions);
        List<DictData> dictDataList = dictDataService.selectDictDataList(8);
        model.addAttribute("infoTypes", dictDataList);
        return CoreConst.ADMIN_PREFIX + "block/form";
    }

    @PostMapping("block/add")
    @ResponseBody
    @CacheEvict(value = "block", allEntries = true)
    public ResponseVo add(InfoBlock block) {
        setBlock(block);
        boolean i = blockService.save(block);
        if (i) {
            return ResultUtil.success("新增信息块成功");
        } else {
            return ResultUtil.error("新增信息块失败");
        }
    }

    @GetMapping("block/edit")
    public String edit(Model model, Integer id) {
        InfoBlock block = blockService.getDetailById(id);
        model.addAttribute("block", block);
        List<InfoPosition> positions = positionService.list();
        model.addAttribute("positions", positions);
        List<DictData> dictDataList = dictDataService.selectDictDataList(8);
        model.addAttribute("infoTypes", dictDataList);
        return CoreConst.ADMIN_PREFIX + "block/form";
    }

    @PostMapping("block/edit")
    @ResponseBody
    @CacheEvict(value = "block", allEntries = true)
    public ResponseVo edit(InfoBlock block) {
        setBlock(block);
        return blockService.updateById(block)
                ?ResultUtil.success("编辑信息块成功")
                :ResultUtil.error("编辑信息块失败");
    }

    private void setBlock(InfoBlock block){
        block.setStatus(CoreConst.STATUS_VALID);
        if(block.getSort() == null){
            block.setSort(255);
        }
        if(block.getParentId() != null && block.getParentId()>0){
            InfoBlock block1 = blockService.getById(block.getParentId());
            block.setParentPath(block1.getParentPath()+block1.getParentId()+",");
        }
    }

    @PostMapping("block/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        if (CollUtil.isNotEmpty(blockService.selectByPid(id))) {
            return ResultUtil.error("该信息下存在子项！");
        }
        return blockService.removeBatchByIds(Arrays.asList(new Integer[] {id}))
                ?ResultUtil.success("删除成功")
                :ResultUtil.error("删除失败");
    }

    @PostMapping("block/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        if (CollUtil.isNotEmpty(blockService.selectByPids(ids))) {
            return ResultUtil.error("待删除的信息下存在子项！");
        }
        return blockService.removeBatchByIds(Arrays.asList(ids))
                ?ResultUtil.success("删除成功")
                :ResultUtil.error("删除失败");
    }
}
