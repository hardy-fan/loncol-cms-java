package com.loncol.cms.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.model.vo.PaginationVo;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.SiteTheme;
import com.loncol.cms.service.SiteThemeService;
import com.loncol.cms.model.vo.PageResultVo;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 后台主题配置
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class SiteThemeController {

    private final SiteThemeService themeService;


    @PostMapping("theme/list")
    @ResponseBody
    public PageResultVo loadTheme(Integer pageNum, Integer pageSize) {
        IPage<SiteTheme> page = new PaginationVo<>(pageNum, pageSize);
        page = themeService.page(page);
        return ResultUtil.table(page.getRecords(), page.getTotal());
    }

    @GetMapping("theme/add")
    public String add() {
        return CoreConst.ADMIN_PREFIX + "theme/form";
    }

    @PostMapping("theme/add")
    @ResponseBody
    public ResponseVo add(SiteTheme theme) {
        theme.setStatus(CoreConst.STATUS_INVALID);
        return themeService.save(theme)
                ?ResultUtil.success("新增主题成功")
                :ResultUtil.error("新增主题失败");
    }

    @GetMapping("theme/edit")
    public String edit(Model model, Integer id) {
        SiteTheme theme = themeService.getById(id);
        model.addAttribute("theme", theme);
        return CoreConst.ADMIN_PREFIX + "theme/form";
    }

    @PostMapping("theme/edit")
    @ResponseBody
    public ResponseVo edit(SiteTheme Theme) {
        return themeService.updateById(Theme)
                ? ResultUtil.success("编辑主题成功")
                : ResultUtil.error("编辑主题失败");
    }

    @PostMapping("theme/use")
    @ResponseBody
    public ResponseVo use(Integer id) {
        int i = themeService.useTheme(id);
        if (i > 0) {
            return ResultUtil.success("启用主题成功");
        } else {
            return ResultUtil.error("启用主题失败");
        }
    }

    @PostMapping("theme/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        boolean result = themeService.removeById(id);

        return (result)
                ? ResultUtil.success("删除主题成功")
                : ResultUtil.error("删除主题失败");
    }

    @PostMapping("theme/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        int i = themeService.deleteBatch(ids);
        return (i > 0)
                ? ResultUtil.success("删除主题成功")
                : ResultUtil.error("删除主题失败");
    }
}
