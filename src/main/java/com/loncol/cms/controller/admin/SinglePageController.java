package com.loncol.cms.controller.admin;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.loncol.cms.common.util.CoreConst;
import com.loncol.cms.common.util.ResultUtil;
import com.loncol.cms.model.po.*;
import com.loncol.cms.service.*;
import com.loncol.cms.model.vo.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 单页管理
 * @author Hardy
 * @date 2022-05-11
 */
@Controller
@AllArgsConstructor
public class SinglePageController {

    private final CategoryService categoryService;
    private final CategoryContentService categoryContentService;

    /**
     * 单页列表
     * @return
     */
    @PostMapping("single/list")
    @ResponseBody
    public List<Category> loadSinglePage() {
        List<Category> categories = categoryService.selectByTypeId(2);
        return categories;
    }

    /**
     * 单页编辑
     * @param model
     * @param id
     * @return
     */
    @GetMapping("single/edit")
    public String edit(Model model, Integer id) {
        CategoryContent categoryContent = categoryContentService.getByCategoryId(id);
        model.addAttribute("single", categoryContent);
        return CoreConst.ADMIN_PREFIX + "single/form";
    }

    @PostMapping("single/edit")
    @ResponseBody
    @CacheEvict(value = "single", allEntries = true)
    public ResponseVo edit(CategoryContent categoryContent) {
        categoryContentService.saveOrUpdate(categoryContent,
                Wrappers.<CategoryContent>lambdaQuery()
                        .eq(CategoryContent::getCategoryId,categoryContent.getCategoryId() ));
        return ResultUtil.success("编辑单页成功");
    }

    @PostMapping("single/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        boolean result = categoryContentService.remove(Wrappers.<CategoryContent>lambdaQuery()
                .eq(CategoryContent::getCategoryId, id));
        return result ? ResultUtil.success("删除单页成功"):ResultUtil.error("删除单页失败");
    }
}
