package com.loncol.cms.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Response对象
 * @author Hardy
 * @date 2022/05/01
 */
@Data
@AllArgsConstructor
public class ResponseVo<T> {

    private Integer status;
    private String msg;
    private T data;

    public ResponseVo(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

}
