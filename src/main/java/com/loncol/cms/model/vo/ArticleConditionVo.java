package com.loncol.cms.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 文章查询条件
 *
 * @author Hardy
 * @date 2022-05-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ArticleConditionVo extends BaseConditionVo {
    private Integer categoryId;
    private Integer tagId;
    private String keywords;
    private Integer status;
    private Boolean random;
    private Boolean recentFlag;

}

