package com.loncol.cms.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * 页面返回结果
 * @author Hardy
 * @date 2022/05/01
 */
@Data
@AllArgsConstructor
public class PageResultVo {
    private List rows;
    private Long total;
}
