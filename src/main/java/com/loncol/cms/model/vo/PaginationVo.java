package com.loncol.cms.model.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 分页类
 *
 * @author Hardy
 * @date 2022-05-05
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class PaginationVo<T> extends Page<T> {
    private static final long serialVersionUID = 5194933845448697148L;
    public PaginationVo(long current, long size) {
        super(current, size);
    }
    private long pages;
    private long prePage;
    private long nextPage;
    private boolean isFirstPage;
    private boolean isLastPage;
    private boolean hasPreviousPage;
    private boolean hasNextPage;

    @Override
    public PaginationVo<T> setTotal(long total) {
        super.setTotal(total);
        pages = super.getPages();
        long current = getCurrent();
        isFirstPage = (current == 1);
        isLastPage = (current == pages || pages == 0);
        hasPreviousPage = current > 1;
        hasNextPage = current < pages;
        prePage = (current > 1)? current - 1: 1;
        nextPage = (current < pages)? current + 1: prePage;
        return this;
    }
}