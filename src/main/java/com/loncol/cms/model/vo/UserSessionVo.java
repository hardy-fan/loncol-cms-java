package com.loncol.cms.model.vo;

import lombok.Data;

/**
 * 用户Session
 * @author Hardy
 * @date 2022/05/01
 */
@Data
public class UserSessionVo {
    private String sessionId;
    private String username;
}
