package com.loncol.cms.model.vo;

import lombok.Data;

/**
 * 修改密码对象
 * @author Hardy
 * @date 2022/05/01
 */
@Data
public class ChangePasswordVo {
    String oldPassword;
    String newPassword;
    String confirmNewPassword;
}
