package com.loncol.cms.model.vo;

import lombok.Data;

/**
 * 评论
 * @author Hardy
 * @date 2022/05/01
 */
@Data
public class CommentConditionVo {
    private String username;
    private Integer articleId;
    private Integer parentId;
    private String qq;
    private String email;
    private String url;
    private Integer status;

}

