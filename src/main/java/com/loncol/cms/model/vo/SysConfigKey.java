package com.loncol.cms.model.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>
 * 系统配置信息
 * </p>
 * @author Hardy
 * @date 2022-05-05
 */
@Getter
@AllArgsConstructor
public enum SysConfigKey {
    /**
     * 云存储配置
     */
    CLOUD_STORAGE_CONFIG("CLOUD_STORAGE_CONFIG", "云存储配置"),
    /**
     * 网站名称
     */
    SITE_NAME("SITE_NAME", "网站名称"),
    /**
     * 网站描述
     */
    SITE_DESC("SITE_DESC", "网站描述"),
    /**
     * 网站关键字
     */
    SITE_KWD("SITE_KWD", "网站关键字"),
    /**
     * 站长名称
     */
    SITE_PERSON_NAME("SITE_PERSON_NAME", "站长名称"),
    /**
     * 站长描述
     */
    SITE_PERSON_DESC("SITE_PERSON_DESC", "站长描述"),
    /**
     * 站长头像
     */
    SITE_PERSON_PIC("SITE_PERSON_PIC", "站长头像");

    /**
     * 值
     */
    private final String value;
    /**
     * 描述
     */
    private final String describe;

}