package com.loncol.cms.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 在线用户
 * @author Hardy
 * @date 2022/05/01
 */
@Data
public class UserOnlineVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String sessionId;
    private String username;
    private String host;
    private Date startTime;
    private Date lastAccess;
    private Date lastLoginTime;
    private long timeout;
    private boolean sessionStatus = Boolean.TRUE;
}
