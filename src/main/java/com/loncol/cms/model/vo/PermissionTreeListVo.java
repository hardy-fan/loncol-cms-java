package com.loncol.cms.model.vo;

import lombok.Data;

/**
 * 权限树形列表值对象
 * @author Hardy
 * @date 2022/05/01
 */

@Data
public class PermissionTreeListVo {
    private Integer id;
    private String permName;
    private Integer parentId;
    private Boolean open = true;
    private Boolean checked = false;
}
