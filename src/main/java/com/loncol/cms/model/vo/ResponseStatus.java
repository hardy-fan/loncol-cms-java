package com.loncol.cms.model.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 响应状态枚举
 * @author Hardy
 * @date 2022-05-05
 */
@Getter
@AllArgsConstructor
public enum ResponseStatus {

    SUCCESS(200, "操作成功！"),
    FORBIDDEN(403, "您没有权限访问！"),
    NOT_FOUND(404, "资源不存在！"),
    ERROR(500, "服务器内部错误！");

    private final Integer code;
    private final String message;

}
