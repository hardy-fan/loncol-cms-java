package com.loncol.cms.model.vo;

import lombok.Data;

/**
 * 页面设置对象
 * @author Hardy
 * @date 2022/05/01
 */
@Data
public class BaseConditionVo {
    private int pageNum = 1;
    private int pageSize = 10;
}
