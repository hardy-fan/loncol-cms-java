package com.loncol.cms.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 上传文件返回
 * @author Hardy
 * @date 2022/05/01
 */
@Data
public class UploadResponse {

    private String fileName;
    private String originalFileName;
    private Long size;
    private String type;
    private String url;
    private Integer status;
    private String msg;

    private UploadResponse(String fileName, String originalFileName, String type, String url, Integer status) {
        this.fileName = fileName;
        this.originalFileName = originalFileName;
        this.type = type;
        this.url = url;
        this.status = status;
    }

    private UploadResponse(String originalFileName, Integer status, String msg) {
        this.originalFileName = originalFileName;
        this.status = status;
        this.msg = msg;
    }

    public static UploadResponse success(String fileName, String originalFileName, String type, String url, Integer status) {
        return new UploadResponse(fileName, originalFileName, type, url, status);
    }

    public static UploadResponse failed(String originalFileName, Integer status, String msg) {
        return new UploadResponse(originalFileName, status, msg);
    }

    @AllArgsConstructor
    public enum ErrorEnum {
        NONE("None"),
        OVER_SIZE("File Size largger than the maximum"),
        ILLEGAL_EXTENSION("Unsupported file type"),
        FILE_NOT_FOUND("File Not Found");

        public final String msg;

    }
}
