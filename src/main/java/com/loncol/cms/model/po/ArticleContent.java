package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 文章内容
 * @TableName cms_article_content
 *
 * @author hardy
 */
@TableName(value ="cms_article_content")
@Data
@Accessors(chain = true)
public class ArticleContent extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 文章编号
     */
    private Integer articleId;

    /**
     * 内容
     */
    private String content;
}