package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 权限
 * @author hardy
 * @TableName auth_permission
 */
@TableName(value ="auth_permission")
@Data
@Accessors(chain = true)
public class Permission extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 父级id
     */
    private Integer parentId;

    /**
     * 资源类型
     */
    private String permType;

    /**
     * 权限名称
     */
    private String permName;

    /**
     * 资源描述
     */
    private String permDesc;

    /**
     * 资源图标
     */
    private String icon;

    /**
     * 资源标识
     */
    private String mark;

    /**
     * 访问路径
     */
    private String url;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态
     */
    private Integer status;

    @TableField(exist = false)
    private List<Permission> children;
}