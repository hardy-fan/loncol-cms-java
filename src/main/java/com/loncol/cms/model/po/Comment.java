package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 评论
 * @author hardy
 */
@TableName(value ="cms_comment")
@Data
@Accessors(chain = true)
public class Comment extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 被评论的文章Id
     */
    private Integer articleId;

    /**
     * 父级评论Id
     */
    private Integer parentId;

    /**
     * 评论者Id
     */
    private Integer userId;

    /**
     * 评论时IP
     */
    private Long userIp;

    /**
     * 评论者账号
     */
    private String username;

    /**
     * 评论者昵称
     */
    private String nickname;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 赞
     */
    private Integer support;

    /**
     * 踩
     */
    private Integer oppose;

    /**
     * 评论人QQ
     */
    private String qq;

    /**
     * 评论人头像
     */
    private String avatar;

    /**
     * 评论人邮箱
     */
    private String email;

    /**
     * 评论状态
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 点赞数
     */
    private Integer likeCount;

    /**
     * 添加时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 父级评论
     */
    @TableField(exist = false)
    Comment parent;
    /**
     * 关联文章
     */
    @TableField(exist = false)
    Article article;
}
