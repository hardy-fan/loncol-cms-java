package com.loncol.cms.model.po;

import java.io.Serializable;

/**
 * @author Hardy
 * @name BaseEntity
 * @date 2022-10-28
 */
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
}
