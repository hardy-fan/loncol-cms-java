package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 文章访问
 * @author fanhu
 * @TableName cms_article_visit
 */
@TableName(value ="cms_article_visit")
@Data
@Accessors(chain = true)
public class ArticleVisit extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 文章编号
     */
    private Integer articleId;

    /**
     * 访问者
     */
    private Integer userId;

    /**
     * 访问者IP
     */
    private Long userIp;

    /**
     * 访问时间
     */
    private Date visitTime;
}