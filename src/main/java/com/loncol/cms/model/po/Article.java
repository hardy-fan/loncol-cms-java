package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 文章
 * @author hardy
 * @data 2022/10/28
 */

@Data
@TableName(value ="cms_article")
@Accessors(chain = true)
public class Article extends BaseEntity {

    /**
     * Id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 栏目类型
     */
    private Integer categoryId;

    /**
     * 文章类型
     */
    private String articleType;

    /**
     * 文章标题
     */
    private String title;

    /**
     * 作者
     */
    private String author;

    /**
     * 封面
     */
    private String cover;

    /**
     * 简介
     */
    private String intro;

    /**
     * 链接
     */
    private String url;

    /**
     * 关键字
     */
    private String keywords;

    /**
     * 二维码
     */
    private String qrcode;

    /**
     * 置顶设置
     */
    private Integer topSet;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 发布时间
     */
    private Date publishDate;

    /**
     * 访问量
     */
    private Integer visitCount;

    /**
     * 点赞数
     */
    private Integer likeCount;

    /**
     * 评论数
     */
    private Integer commentCount;

    /**
     * 开启评论
     */
    private Integer commentFlag;

    /**
     * 原创标志
     */
    private Integer originalFlag;

    /**
     * 推荐标志
     */
    private Integer recommentFlag;

    /**
     * 添加时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 创建者
     */
    private Integer createUser;

    /**
     * 修改者
     */
    private Integer updateUser;

    /**
     * 删除标志
     */
    private Integer deleted;

    /**
     * 文章内容
     * 来自ArticleContent表
     */
    @TableField(exist = false)
    private String content;

    /**
     * 新帖标志
     */
    @TableField(exist = false)
    private Integer newFlag = 0;

    /**
     * 热帖标志
     *
     */
    @TableField(exist = false)
    private Integer hotFlag = 0;

    /**
     * 标签列表
     */
    @TableField(exist = false)
    List<Tag> tags = null;

    /**
     * 所属栏目
     */
    @TableField(exist = false)
    Category category;
}