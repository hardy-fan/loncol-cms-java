package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 点赞
 * @author hardy
 * @date 2022/10/30
 */
@TableName(value ="cms_article_like")
@Data
public class ArticleLike extends BaseEntity {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 文章编号
     */
    private Integer articleId;

    /**
     * 访问者
     */
    private Integer userId;

    /**
     * 访问者IP
     */
    private Long userIp;

    /**
     * 添加时间
     */
    private Date likeTime;
}