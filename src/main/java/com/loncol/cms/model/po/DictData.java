package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字典数据
 * @TableName sys_dict_data
 */
@TableName(value ="sys_dict_data")
@Data
@Accessors(chain = true)
public class DictData extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 类型编号
     */
    private Integer typeId;

    /**
     * 上级代码
     */
    private String parentCode;

    /**
     * 字典代码
     */
    private String code;

    /**
     * 字典名称
     */
    private String name;

    /**
     * 是否激活
     */
    private Integer status;
}