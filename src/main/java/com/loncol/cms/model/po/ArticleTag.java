package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 文章标签
 * @author hardy
 */
@TableName(value ="cms_article_tag")
@Data
@Accessors(chain = true)
public class ArticleTag extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 文章编号
     */
    private Integer articleId;

    /**
     * 标签标号
     */
    private Integer tagId;

    /**
     * 添加时间
     */
    private Date tagTime;

}