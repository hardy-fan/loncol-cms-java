package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 网站统计
 * @author hardy
 * @TableName sys_site_stat
 */
@TableName(value ="sys_site_stat")
@Data
@Accessors(chain = true)
public class SiteStat extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 栏目数
     */
    private Integer categoryCount;
    /**
     * 文章数
     */
    private Integer articleCount;
    /**
     * 标签数
     */
    private Integer tagCount;
    /**
     * 浏览量
     */
    private Integer visitCount;
    /**
     * 点赞量
     */
    private Integer likeCount;
    /**
     * 评论量
     */
    private Integer commentCount;
}