package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 信息块
 * @TableName cms_info_block
 */
@TableName(value ="cms_info_block")
@Data
@Accessors(chain = true)
public class InfoBlock extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 上级编号
     */
    private Integer parentId;

    /**
     * 上级路径
     */
    private String parentPath;

    /**
     * 推荐位
     */
    private Integer positionId;

    /**
     * 推荐位描述
     */
    @TableField(exist = false)
    private String positionName;

    /**
     * 类型
     */
    private String infoType;

    /**
     * 类型描述
     */
    @TableField(exist = false)
    private String infoTypeName;

    /**
     * 标题
     */
    private String title;

    /**
     * 简介
     */
    private String intro;

    /**
     * 图片
     */
    private String image;

    /**
     * 链接
     */
    private String url;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否激活
     */
    private Integer status;

    @TableField(exist = false)
    private InfoBlock parent;

    @TableField(exist = false)
    private List<InfoBlock> children;
}