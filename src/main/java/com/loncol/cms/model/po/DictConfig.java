package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字典配置
 * @author hardy
 */
@TableName(value ="sys_dict_config")
@Data
@Accessors(chain = true)
public class DictConfig extends BaseEntity {
    /**
     * 编号
     */
    private Integer id;

    /**
     * 配置代码
     */
    private String configCode;

    /**
     * 配置名称
     */
    private String configName;

    /**
     * 数据来源
     */
    private String dataFrom;

    /**
     * 数据定义
     */
    private String dataDefine;

    /**
     * 是否激活
     */
    private Integer status;
}