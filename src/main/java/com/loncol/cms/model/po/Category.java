package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 栏目
 * @author fanhu
 * @TableName cms_category
 */
@TableName(value ="cms_category")
@Data
@Accessors(chain = true)
public class Category extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 上级编号
     */
    private Integer parentId;
    /**
     * 上级路径
     */
    private String parentPath;
    /**
     * 栏目类型
     */
    private String categoryType;
    /**
     * 栏目名称
     */
    private String categoryName;
    /**
     * 链接地址
     */
    private String url;
    /**
     * 栏目介绍
     */
    private String intro;
    /**
     * 栏目图标
     */
    private String icon;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 是否激活
     */
    private Integer status;
    /**
     * 父栏目
     */
    @TableField(exist = false)
    private Category parent;
    /**
     * 子栏目列表
     */
    @TableField(exist = false)
    private List<Category> children;
}