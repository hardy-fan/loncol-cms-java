package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 信息位
 * @TableName cms_info_position
 */
@TableName(value ="cms_info_position")
@Data
@Accessors(chain = true)
public class InfoPosition extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 推荐位名称
     */
    private String positionName;

    /**
     * 推荐位描述
     */
    private String positionDesc;

    /**
     * 状态
     */
    private Integer status;
}