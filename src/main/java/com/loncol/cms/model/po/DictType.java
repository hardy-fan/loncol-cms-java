package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字典类型
 * @TableName sys_dict_type
 */
@TableName(value ="sys_dict_type")
@Data
@Accessors(chain = true)
public class DictType extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 类型编号
     */
    private Integer typeId;

    /**
     * 类型名称
     */
    private String typeName;

    /**
     * 是否激活
     */
    private Integer status;
}