package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 网站配置
 * @author hardy
 * @TableName sys_site_config
 */
@TableName(value ="sys_site_config")
@Data
@Accessors(chain = true)
public class SiteConfig extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 键
     */
    private String siteKey;
    /**
     * 值
     */
    private String siteVal;
    /**
     * 描述
     */
    private String siteDesc;
    /**
     * 状态
     */
    private Integer status;
}