package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 系统主题
 * @author hardy
 * @TableName sys_site_theme
 */
@TableName(value ="sys_site_theme")
@Data
@Accessors(chain = true)
public class SiteTheme extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 主题名
     */
    private String themeName;

    /**
     * 主题描述
     */
    private String themeDesc;

    /**
     * 主题预览图
     */
    private String themeIcon;

    /**
     * 是否激活
     */
    private Integer status;
}