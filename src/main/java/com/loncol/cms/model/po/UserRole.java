package com.loncol.cms.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用户角色
 * @author Hardy
 */
@TableName(value ="auth_user_role")
@Data
@Accessors(chain = true)
public class UserRole extends BaseEntity {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户账号
     */
    private Integer userId;

    /**
     * 角色id
     */
    private Integer roleId;
}