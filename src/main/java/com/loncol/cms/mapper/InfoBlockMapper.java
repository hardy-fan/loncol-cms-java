package com.loncol.cms.mapper;

import com.loncol.cms.model.po.InfoBlock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * 信息块 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface InfoBlockMapper extends BaseMapper<InfoBlock> {
    /**
     * 查询树形结构
     * @param query 条件主要是推荐位
     * @return
     */
    List<InfoBlock> selectBlocks(InfoBlock query);

    /**
     * 获取栏目信息
     * 包括其父栏目
     * @param id 编号
     * @return
     */
    InfoBlock getDetailById(Integer id);
}




