package com.loncol.cms.mapper;

import com.loncol.cms.model.po.ArticleContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 文章内容 Mapper
 * @author hardy
 * @date 2022-10-30
*/
public interface ArticleContentMapper extends BaseMapper<ArticleContent> {
}




