package com.loncol.cms.mapper;

import com.loncol.cms.model.po.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色权限 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}




