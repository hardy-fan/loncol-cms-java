package com.loncol.cms.mapper;

import com.loncol.cms.model.po.DictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 字典类型 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface DictTypeMapper extends BaseMapper<DictType> {

}




