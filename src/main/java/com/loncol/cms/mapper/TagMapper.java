package com.loncol.cms.mapper;

import com.loncol.cms.model.po.Tag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 标签 Mapper
 * @author Hardy
 * @date 2022-10-30
*/

public interface TagMapper extends BaseMapper<Tag> {

}




