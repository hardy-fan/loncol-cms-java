package com.loncol.cms.mapper;

import com.loncol.cms.model.po.InfoPosition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 信息位 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface InfoPositionMapper extends BaseMapper<InfoPosition> {

}




