package com.loncol.cms.mapper;

import com.loncol.cms.model.po.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface RoleMapper extends BaseMapper<Role> {

}




