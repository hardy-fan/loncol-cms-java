package com.loncol.cms.mapper;

import com.loncol.cms.model.po.ArticleTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 文章标签
 * @author Hardy
 * @date 2022-10-30
*/
public interface ArticleTagMapper extends BaseMapper<ArticleTag> {

}




