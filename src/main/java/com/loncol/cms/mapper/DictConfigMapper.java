package com.loncol.cms.mapper;

import com.loncol.cms.model.po.DictConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 字典配置 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface DictConfigMapper extends BaseMapper<DictConfig> {
}




