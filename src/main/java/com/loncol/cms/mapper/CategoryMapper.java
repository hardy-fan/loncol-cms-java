package com.loncol.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.loncol.cms.model.po.Category;

import java.util.List;

/**
 * 栏目 Mapper
 * @author Hardy
 * @date 2022-10-30
 */
public interface CategoryMapper extends BaseMapper<Category> {
    /**
     * 获得栏目列表
     * 包括其父子栏目信息
     * @param categoryCondition 查询条件
     * @return
     */
    List<Category> selectCategories(Category categoryCondition);

    /**
     * 获取栏目信息
     * 包括其父栏目
     * @param id
     * @return
     */
    Category getDetailById(Integer id);
}




