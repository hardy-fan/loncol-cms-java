package com.loncol.cms.mapper;

import com.loncol.cms.model.po.ArticleVisit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 文章访问 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface ArticleVisitMapper extends BaseMapper<ArticleVisit> {

}




