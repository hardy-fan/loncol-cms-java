package com.loncol.cms.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.model.po.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.loncol.cms.model.vo.ArticleConditionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文章 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface ArticleMapper extends BaseMapper<Article> {

    /**
     * 文章分页列表
     * 查询条件中如果有TagId，将会按照tahId进行过滤
     * @param page 分页设置
     * @param vo 查询条件
     * @return
     */
    List<Article> findByCondition(@Param("page") IPage<Article> page, @Param("vo") ArticleConditionVo vo);
}




