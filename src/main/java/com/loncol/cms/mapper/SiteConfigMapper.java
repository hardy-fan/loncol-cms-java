package com.loncol.cms.mapper;

import com.loncol.cms.model.po.SiteConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 网站配置 Mapper
 * @author Hardy
 * @date 2022-10-30
*/

public interface SiteConfigMapper extends BaseMapper<SiteConfig> {

}




