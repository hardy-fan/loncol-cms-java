package com.loncol.cms.mapper;

import com.loncol.cms.model.po.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户角色 Mapper
 * @author Hardy
 * @date 2022-10-30
*/

public interface UserRoleMapper extends BaseMapper<UserRole> {

}




