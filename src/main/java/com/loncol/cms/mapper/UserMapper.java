package com.loncol.cms.mapper;

import com.loncol.cms.model.po.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户 Mapper
 * @author Hardy
 * @date 2022-10-30
*/

public interface UserMapper extends BaseMapper<User> {
}




