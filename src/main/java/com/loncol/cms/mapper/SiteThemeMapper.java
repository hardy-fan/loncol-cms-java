package com.loncol.cms.mapper;

import com.loncol.cms.model.po.SiteTheme;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统主题 Mapper
 * @author Hardy
 * @date 2022-10-30
*/

public interface SiteThemeMapper extends BaseMapper<SiteTheme> {

}




