package com.loncol.cms.mapper;

import com.loncol.cms.model.po.SiteStat;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 网站统计 Mapper
 * @author Hardy
 * @date 2022-10-30
*/

public interface SiteStatMapper extends BaseMapper<SiteStat> {

}




