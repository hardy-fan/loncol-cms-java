package com.loncol.cms.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.loncol.cms.model.po.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.loncol.cms.model.vo.CommentConditionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 评论 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface CommentMapper extends BaseMapper<Comment> {
    /**
     * 根据条件获得评论分页列表
     * @param page 分页对象
     * @param vo 查询条件对象
     * @return
     */
    List<Comment> selectComments(@Param("page") IPage<Comment> page, @Param("vo") CommentConditionVo vo);

    /**
     * 批量删除
     * @param ids 编号列表
     * @return
     */
    int deleteBatch(Integer[] ids);
}




