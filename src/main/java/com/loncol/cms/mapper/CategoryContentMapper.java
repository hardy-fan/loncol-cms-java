package com.loncol.cms.mapper;

import com.loncol.cms.model.po.CategoryContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 栏目内容 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface CategoryContentMapper extends BaseMapper<CategoryContent> {

}




