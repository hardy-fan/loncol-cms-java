package com.loncol.cms.mapper;

import com.loncol.cms.model.po.DictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 字典数据 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface DictDataMapper extends BaseMapper<DictData> {

}




