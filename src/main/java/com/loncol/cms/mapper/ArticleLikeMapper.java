package com.loncol.cms.mapper;

import com.loncol.cms.model.po.ArticleLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 点赞 Mapper
 *
 * @author Hardy
 * @date 2022-10-30
*/
public interface ArticleLikeMapper extends BaseMapper<ArticleLike> {

}




