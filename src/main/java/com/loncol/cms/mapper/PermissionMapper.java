package com.loncol.cms.mapper;

import com.loncol.cms.model.po.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Set;

/**
 * 权限 Mapper
 * @author Hardy
 * @date 2022-10-30
*/
public interface PermissionMapper extends BaseMapper<Permission> {
    /**
     * 获得用户的所有资源
     * @param userId 用户编号
     * @return
     */
    Set<String> findMarksByUserId(Integer userId);

    /**
     * 获得菜单列表
     * @param userId 用户编号
     * @return
     */
    List<Permission> selectMenuByUserId(Integer userId);

}




