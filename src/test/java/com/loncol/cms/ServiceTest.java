/**
 * <p>
 *
 * </p>
 *
 * @author Hardy
 * @since 2022-05-05
 */
package com.loncol.cms;

import com.loncol.cms.model.po.Category;
import com.loncol.cms.service.CategoryService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
public class ServiceTest {
    @Resource
    private CategoryService categoryService;


    @Test
    public void CategoryTest(){
        Category categoryCond = new Category();
        categoryCond.setCategoryName("技术");
        List<Category> categories = categoryService.selectCategories(categoryCond);
        for (Category category:categories){
            System.out.println(category.getCategoryName());
        }
    }

}
