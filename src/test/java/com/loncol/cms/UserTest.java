package com.loncol.cms;

import cn.hutool.core.lang.Console;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.loncol.cms.model.po.User;
import com.loncol.cms.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Hardy
 * @date 2022-06-13
 */

@SpringBootTest
public class UserTest {

    @Resource
    private UserService userService;

    /**
     * 重置admin的密码
     */
    @Test
    public void setPassword(){
        User user = new User();
        user.setUsername("admin");
        user.setPassword("123456");
        userService.encryptPassword(user);
        String sysOldPassword = user.getPassword();
        User newUser = ObjectUtil.clone(user);
        System.out.println(newUser);
        newUser.setPassword("123456");
        String entryOldPassword = userService.getEncryptPassword(newUser);
        System.out.println(sysOldPassword);
        System.out.println(entryOldPassword);

    }

    @Test
    public void md5Test() {
        Console.log(DigestUtil.md5Hex("fanhuayi@loncol.com"));
    }
}
