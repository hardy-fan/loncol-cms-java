# Loncol CMS 项目说明

Loncol CMS内容管理系统，采用SpringBoot+Mybatis Plus+Thymeleaf+Apache Shiro，数据库采用Mysql，数据库连接池使用druid，数据缓存采用Redis，文件存储采用阿里云OSS。

也使用了Hutool等工具类。

前端使用bootstrap4、adminlte

编辑器使用ckeditor4

上传组件uploader

可以作为个人博客或者企业网站使用。提供了栏目、文章、标签、推荐、友情链接、轮播图、评论、点赞、统计等功能。